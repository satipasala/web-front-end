
// Common elements
export const addNewItemButton = "#addNewItem";
export const closeSubNavButton = "#closeSubNav";
export const addressStreetNameInput = '#streetName';
export const addressCityNameInput = '#cityName';
export const contextMenuEditButton = '#contextMenuEditBtn';

// Login elements
export const loginEmailInput = '#userEmail';
export const loginPasswordInput = '#userPassword'
export const loginButton = '#userLoginButton';
export const logoutMenuButton = "#userLogoutMenuButton";
export const logoutButton = "#userLogoutButton";

// Ref data elements
export const organizationTypeNameInput = "#orgTypeName";
export const organizationTypeDescriptionInput = "#orgTypeDescription";
export const organizationLocationNameInput = '#orgLocationName';
export const organizationLocationDisplayNameInput = '#orgLocationDisplayName';
export const addOrganizationLocationButton = '#saveOrgLocation';
export const activityTypeNameInput = "#activityTypeName";
export const activityTypeDescriptionInput = "#activityDescription";
export const questionTypeNameInput = '#questionTypeName';
export const questionTypeInput = '#questionType';
export const questionTypeLabelInput = '#questionTypeLabel';
export const questionTypeAnswerTypeInput = '#questionTypeAnswerType';
export const questionLabelNameInput = '#questionLabelName';
export const questionLabelTypeInput = '#questionLabelType';
export const questionLabelCategoryInput = '#questionLabelCategory';
export const languageNameInput = '#languageName';
export const languageTypeInput = '#languageType';
export const languageShortNameInput = '#languageShortName';
export const stateNameInput = '#stateName';
export const stateShortNameInput = '#stateShortName';
export const stateCountryInput = '#stateCountry';
export const stateDescriptionInput = '#stateDescription';
export const cityNameInput = '#cityName';
export const cityCountryInput = '#cityCountry';
export const cityStateInput = '#cityState';
export const cityLatitudeInput = '#cityLatitude';
export const cityLongitudeInput = '#cityLongitude';
export const eventCategoryNameInput = '#eventCategoryName';
export const eventCategoryDescriptionInput = '#eventCategoryDescription';

// Auth elements
export const authRoleNameInput = '#roleName';
export const authRoleLevelInput = '#roleLevel';
export const authRoleOrganizationTypeInput = '#roleOrganizationType';
export const authRoleViewPermissionLevelInput = '#roleViewPermissionLevel';
export const authRoleEditPermissionLevelInput = '#roleEditPermissionLevel';

// Questionnaire elements
export const newQuestionNameInput = '#questionName';
export const newQuestionTypeInput = '#questionType';
export const newQuestionDescriptionInput = '#questionDescription';
export const questionnaireNameInput = '#questionnaireName';

// Organization elements
export const organizationNameInput = '#organizationName';
export const organizationDescriptionInput = '#organizationDescription';
export const organizationTypeInput = '#organizationType';
export const organizationMediumInput = '#preferredMedium';
export const organizationPhoneNumberInput = '#organizationPhoneNumber';
export const organizationRegNumberInput = '#organizationRegNumber';
export const organizationWebsiteInput = '#organizationWebsite';
export const organizationEmailInput = '#organizationEmail';
export const personInChargeNameInput = '#personInChargeName';
export const personInChargeDesignationInput = '#personInChargeDesignation';
export const personInChargePhoneInput = '#personInChargePhone';
export const personInChargeEmailInput = '#personInChargeEmail';
export const addHostLocationButton = '#addHostLocation';








