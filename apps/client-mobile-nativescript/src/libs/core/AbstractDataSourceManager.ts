import {Input, OnDestroy} from "@angular/core";
import {map, scan} from "rxjs/operators";
import {ArrayUtils} from "../../../../../libs/base/src/lib/utils/array-utils";
import {SDataSource} from "../../../../../libs/base/src/lib/impl/SDataSource";
import {FirebaseDataSource, FilterGroup, OrderBy} from "../base/impl/FirebaseDataSource";
import {SearchFilter} from "../../../../../libs/base/src/lib/model/SearchFilter";
import {LocalDataSource} from "../base/impl/LocalDataSource";
import {InfiniteScrollService} from "../base/services/InfiniteScrollService";
import {Observable, of} from "rxjs";
import {ObservableArray} from "@nativescript/core";

export abstract class AbstractDataSourceManager implements OnDestroy {

  get dataSource(): SDataSource<any> {
    return this._dataSource;
  }

  @Input()
  set dataSource(value: SDataSource<any>) {
    this._dataSource = value;
  }


  private _collectionName: string;

  get collectionName(): string {
    return this._collectionName;
  }

  @Input()
  set collectionName(value: string) {
    this._collectionName = value;
    this.collectionService.setCollection(value);
    this._dataSource = new FirebaseDataSource<any>(null, null, this.collectionService);
  }

  private _dataSet: Array<any>;

  private _orderBy: OrderBy[];

  private _filterBy: FilterGroup[];

  private _searchBy: SearchFilter[];

  get dataSet(): Array<any> {
    return this._dataSet;
  }

  @Input()
  set dataSet(value: Array<any>) {
    this._dataSet = value;
    if (this._dataSource != null) {
      this._dataSource = null;
      // throw "Data source is already set.Setting both data set and DataSource is not allowed."
    }
    this._dataSource = new LocalDataSource(value);
  }

  @Input()
  set dataArray(value: Array<any>) {
    this.infinite = new ObservableArray<any>(value);
    this._dataSource = new LocalDataSource(value);
  }

  get orderBy(): OrderBy[] {
    return this._orderBy;
  }

  @Input()
  set orderBy(value: OrderBy[]) {
    this._orderBy = value;
    if (value != null) {
      this._dataSource.setOrderBy(...value);
    }
    this._connectToDatabase();
  }

  get filterBy(): FilterGroup[] {
    return this._filterBy;
  }

  @Input()
  set filterBy(value: FilterGroup[]) {
    this._filterBy = value;
    if (value != null) {
      this._dataSource.setFilterGroups(value);
      this._connectToDatabase();
    }
  }


  get searchBy(): SearchFilter[] {
    return this._searchBy;
  }

  @Input()
  set searchBy(value: SearchFilter[]) {
    this._searchBy = value;
    if (value != null) {
      this._dataSource.setSearchFilters(value);
      this._connectToDatabase();
    }
  }

  private _batchSize;

  get batchSize(): number {
    return this._batchSize;
  }

  @Input()
  set batchSize(value: number) {
    this._batchSize = value;
    this.dataSource.setBatchSize(value);
  }

  infinite: ObservableArray<any> = new ObservableArray<any>();

  private _dataSource: SDataSource<any>;

  protected constructor(protected collectionService: InfiniteScrollService) {

  }


  ngOnDestroy() {
    this._dataSource.disconnect();
  }

  getBatch() {
    this._dataSource.nextBatch();
  }

  abstract nextBatch(offset);

  clearAllFilters() {
    this._dataSource.clearAllFilters();
    this._connectToDatabase();
  }

  /*private _connectToDatabase() {

   this.infinite =  this._dataSource.connect().pipe(scan((acc, batch) => {
      //create or update existing scanned object to avoid duplicates. object keys are ids of the documents.
      batch.forEach(value => {
        //todo name check if id is not found should be fixed.
        value.id ? acc[value.id] = value : acc[value.name] = value;
      });
      //todo optimize order by taking only last batch and adding it to already sorted array.(consider dynamic updates too)
      return acc;
    }, {}), map(obj => {
      let values = Object.keys(obj).map(k => obj[k]) // changes due to use of es6
      return ArrayUtils.sortArray(values, this.orderBy)
    }));
  }*/

  private _connectToDatabase() {
    this._dataSource.connect().subscribe(next => {
      this.infinite.push(next)
    })
  }
}
