import * as firebase from "nativescript-plugin-firebase";
import {Injectable, NgZone} from "@angular/core";
import {Observable, Subject} from "rxjs";
import {firestore} from "nativescript-plugin-firebase";
import CollectionReference = firestore.CollectionReference;
import Query = firestore.Query;
import QueryDocumentSnapshot = firestore.QueryDocumentSnapshot;
import QuerySnapshot = firestore.QuerySnapshot;

export declare type QueryFn = (ref: CollectionReference) => Query;

@Injectable()
export abstract class CollectionService<T> {

  reachedEnd: boolean = false;
  lastDoc: QueryDocumentSnapshot;
  collection: string;
  fireStore = firebase.firestore;

  protected constructor(protected collectionName: string, protected _ngZone: NgZone) {
    this.collection = collectionName;
  }

  public setCollection(collectionName: string) {
    this.collection = collectionName
  }

  // public add(doc: T): Promise<firebase.firestore.DocumentReference> {
  //   return this.fireStore.collection(this.collection).add(doc);
  // }

  // public addWithId(doc: any): Promise<void> {
  //   return this.fireStore.collection(this.collection).doc(doc.id).set(doc);
  // }

  // public update(id, doc: T): Promise<void> {
  //   return this.fireStore.collection(this.collection).doc(id).update(doc);
  // }

  // public delete(id): Promise<void> {
  //   return this.fireStore.collection(this.collection).doc(id).delete();
  // }

  /**
   * get the collection reference of given collection name.
   * @param documentId
   * @param queryFn
   */
  public get(documentId: string, queryFn?: QueryFn): Observable<T> {
    return Observable.create(observer => {
      let collectionRef: CollectionReference;
      if (queryFn) {
        collectionRef = queryFn.apply(this.fireStore.collection(this.collection))
      } else {
        collectionRef = this.fireStore.collection(this.collection);
      }

      collectionRef.doc(documentId).onSnapshot((actions) => {
        observer.next(this.getDocumentData(actions));
        observer.complete();
      }, (error) => {
        observer.error(error);
      })
    });
  }


  private getDocumentData(payload: firebase.firestore.DocumentSnapshot): T {
    const data: T = payload.data() as T;
    if (data) {
      data['id'] = payload.id as string;
    }
    return data;
  }

  /**
   * get data from document change action.
   * @param action
   */
  private getPayload(action: QueryDocumentSnapshot): T {
    if (action.data() != null) {
      const data: T = action.data() as T;
      data['id'] = action.id as string;
      this.lastDoc = action;
      return data
    } else {
      const u1: T = null;
      return u1;
    }
  }

  /**
   * get a sumb collection of main collection
   * @param subCollectionPath
   * @param documentId
   * @param queryFn
   */
  public queryCollection(queryFn: QueryFn): Subject<T[]> {
    const dataSubject: Subject<T[]> = new Subject<T[]>();

    const collection: CollectionReference = queryFn.apply(this.fireStore.collection(this.collection)) ;

    return this.onSnapshotChanges(collection, dataSubject);
  }


  /**
   * get a sumb collection of main collection
   * @param subCollectionPath
   * @param documentId
   * @param queryFn
   */
  public querySubCollection(queryFn: QueryFn, ...subCollectionPaths: SubCollectionInfo[]): Subject<T[]> {
    let collection: CollectionReference = null;

    const dataSubject: Subject<T[]> = new Subject<T[]>();

    if (subCollectionPaths.length > 0) {
      for (let i = 0; i < subCollectionPaths.length; i++) {
        if (collection) {
          collection = this.getSubCollection(collection, subCollectionPaths[i])
        } else {
          collection = this.getSubCollection(this.fireStore.collection(this.collection), subCollectionPaths[i])
        }
      }

    } else {
      collection = this.fireStore.collection(this.collection);
    }


    return this.onSnapshotChanges(collection, dataSubject);
  }

  private onSnapshotChanges(collection: CollectionReference, dataSubject) {
    collection.onSnapshot((snapshot: QuerySnapshot) => {
      // snapshot.docSnapshots.length !== 0 ? (this.reachedEnd = true) : (this.reachedEnd = false);
      snapshot.forEach(docSnap => dataSubject.next(this.getPayload(docSnap)));
    });
    return dataSubject;
  }

  /**
   * get a sub collection from a given collection
   * @param collection
   * @param subCollection sub collection info along with document id
   */
  private getSubCollection(collection: CollectionReference, subCollection: SubCollectionInfo): CollectionReference {
    return collection.doc(subCollection.documentId).collection(subCollection.subCollection)
  }

  /**
   * Return all documents in the collection
   * @returns {Observable<T[]>}
   */
  public getAll(): Observable<T[]> {
    let subject: Subject<T[]> = new Subject<T[]>();
    this.fireStore.collection(this.collection).onSnapshot((snapshot: firebase.firestore.QuerySnapshot) => {
      let data = [];
      snapshot.forEach(docSnap => data.push(docSnap.data()));
      subject.next(data);
    });
    return subject.asObservable();
  }

  public delete(id): Promise<void> {
    return this.fireStore.collection(this.collection).doc(id).delete();
  }


  // public setDoc(id, doc: T): Promise<void> {
  //    return this.fireStore.collection(this.collection).doc(id).set(doc);
  //  }
}

/**
 * sub collection info used for sub collection retrieval
 */
export interface SubCollectionInfo {
  documentId: string,
  subCollection: string;
}
