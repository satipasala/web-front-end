import {Injectable, NgZone} from '@angular/core';
import {CollectionService} from "../impl/CollectionService";

@Injectable()
export class InfiniteScrollService extends CollectionService<any> {

  constructor(protected _ngZone: NgZone) {
    super(undefined, _ngZone);
  }


}
