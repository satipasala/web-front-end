import { Injectable } from "@angular/core";
import { Course } from "../../../../../../libs/base/src/lib/model/Course";

@Injectable()
export class AppStateService {

  private selectedCourse : Course;

  setSelectedCourse(course : Course){
    this.selectedCourse = course;
  }

  getSelectedCourse(){
    return this.selectedCourse;
  }
}
