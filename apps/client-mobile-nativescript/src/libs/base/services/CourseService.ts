import {CollectionService} from "../impl/CollectionService";
import {Injectable, NgZone} from "@angular/core";
@Injectable()
export class CourseService extends CollectionService<any>{
  public static collection: string = "courses";

  constructor(protected _ngZone: NgZone){
    super(CourseService.collection, _ngZone)
  }

}
