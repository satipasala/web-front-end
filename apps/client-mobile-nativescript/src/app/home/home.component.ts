import {Component, OnInit, ViewChild} from '@angular/core';
import * as  app from "tns-core-modules/application";
import * as  color from "tns-core-modules/color";
import * as  platformModule from "tns-core-modules/platform";
import * as  orientationModule from "nativescript-screen-orientation";
import { CourseService } from '../../libs/base/services/CourseService';
import { Course } from '../../../../../libs/base/src';
import { AuthService } from '../../libs/base/services/AuthService';
import { NsPermissionsService } from '../../libs/base/services/ns-permissions.service';


declare var android: any;
declare var dialogs: any;
declare var Frame: any;

@Component({
  selector: 'ns-home-component',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  @ViewChild("dot-one", {static: false}) d1: any;
  @ViewChild("dot-two", {static: false}) d2: any;
  @ViewChild("slider-wrapper", {static: false}) sliderWrapper: any;

  status = "not scrolling";
  b_slider = 0;
  c_type = "a";

  barSource = [
    {Day: "Mon", Time: 0,ID:0},
    {Day: "Tue", Time: 0,ID:0},
    {Day: "Wed", Time: 0,ID:0},
    {Day: "Thu", Time: 0,ID:0},
    {Day: "Fri", Time: 0,ID:0},
    {Day: "Sat", Time: 0,ID:0},
    {Day: "Sun", Time: 0,ID:0}
  ];
  dashboardOpen = false;
  dashboard_wrapper_classes = "dashboard-wrapper jump-up";
  ico_class = "ico";
  dash_text = ['This week meditation stats', 'View your weekly stats'];
  dash_ico = "";

  courses: Course[];

  constructor(private permissionService : NsPermissionsService ,
              private authService : AuthService,
              private _courseService: CourseService) {
    // this.authService.getCurrentDbUser().subscribe(user => {
    //   this.courses = user['courseSubscriptions'];
    // });
    this._courseService.getAll().subscribe(coursesArr => {
      this.courses = coursesArr;
    });

  }

  change(args) {
    this.ico_class = "ico";
    // args.object.className = "ico g";
  }

  pop_msg() {
    dialogs.alert({
      title: "Silence is Golden",
      okButtonText: "OK"
    });
  }

  random_course() {
    var rand = Math.floor(Math.random() * Math.floor(2));
    if (rand == 0) {
      this.c_type = "basic"
    }
    else {
      this.c_type = "advance";
    }

    Frame.topmost().navigate({
      moduleName: "meditation-course/meditation-course",
      animated: true,
      context: {course_type: this.c_type},
      transition: {
        name: "fade",
        duration: 380,
        curve: "easeIn"
      }
    });

  }

  bannerPressed() {
    this.c_type = "basic";
    Frame.topmost().navigate({
      moduleName: "meditation-course/meditation-course",
      animated: true,
      context: {course_type: this.c_type},
      transition: {
        name: "fade",
        duration: 380,
        curve: "easeIn"
      }
    });
  }

  bannerPressed2() {
    this.c_type = "advance";
    Frame.topmost().navigate({
      moduleName: "meditation-course/meditation-course",
      animated: true,
      context: {course_type: this.c_type},
      transition: {
        name: "fade",
        duration: 380,
        curve: "easeIn"
      }
    });
  }

  showDashboard() {
    if (this.dashboardOpen) {
      this.dash_ico = "";
      this.dashboardOpen = false;
      this.dashboard_wrapper_classes = "dashboard-wrapper";
    }
    else {
      this.dash_ico = "";
      this.dashboardOpen = true;
      this.dashboard_wrapper_classes = "dashboard-wrapper";
      this.barSource = [
        {Day: "Mon", Time: 12, ID: 1},
        {Day: "Tue", Time: 9, ID: 2},
        {Day: "Wed", Time: 14, ID: 3},
        {Day: "Thu", Time: 11, ID: 4},
        {Day: "Fri", Time: 13, ID: 5},
        {Day: "Sat", Time: 15.3, ID: 6},
        {Day: "Sun", Time: 10, ID: 7}
      ];
    }

  }

  ngOnInit() {
  }


  pageLoaded(args) {

    var page = args.object;

    /* custom func. to add OS class & HD/Non-HD class */
    var phone_height = platformModule.screen.mainScreen.heightPixels;
    var isHD = 'no-hd';
    if (phone_height > 1920) {
      isHD = 'hd';
    }
    page.className = page.className + " " + isHD;

    if (platformModule.isIOS) {
      page.getViewById('main_scroll').ios.bounces = false;
      page.className = page.className + " ios";
      // args.object.frame.ios.controller.navigationBar.barStyle = 0;
    }

    if (platformModule.isAndroid) {
      page.className = page.className + " android";
    }

    if (platformModule.isAndroid && platformModule.device.sdkVersion >= '23') {
      var View = android.view.View;
      const window = app.android.foregroundActivity.getWindow();
      window.setStatusBarColor(new color.Color("#E3E8F1").android);
      window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
      window.addFlags(android.view.WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.clearFlags(android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      window.setNavigationBarColor(new color.Color('#D9E0EC').android);
    }
    /* custom function end*/


    orientationModule.setCurrentOrientation("portrait", args => {
      console.log(args);
    });

    this.b_slider = this.sliderWrapper?.getActualSize().width;

    this.d2 = page.getViewById('dot-two');
  }

  onNavigatingFrom() {
    orientationModule.orientationCleanup();
  }

  onScroll(args) {

    if ((this.b_slider / 2) < args.scrollX) {
      this.d1.className = "";
      this.d2.className = "active";
    }
    else {
      this.d1.className = "active";
      this.d2.className = "";
    }
  }

}
