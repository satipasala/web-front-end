import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule, NativeScriptFormsModule } from "@nativescript/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";

// Uncomment and add to NgModule imports if you need to use two-way binding
// import { NativeScriptFormsModule } from "nativescript-angular/forms";

// Uncomment and add to NgModule imports if you need to use the HttpClient wrapper
// import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { LoginComponentComponent } from './login-component/login-component.component';
import { HomeComponent } from "./home/home.component";
import { OptionalCourseCardComponent } from "./courses/course-card/optional-course-card/optional-course-card.component";
import { OptionalCourseListComponent } from "./courses/course-list/optional-course-list/optional-course-list.component";
import { CourseService } from "../libs/base/services/CourseService";
import { CommonModule } from "@angular/common";
import { MandatoryCourseListComponent } from "./courses/course-list/mandatory-course-list/mandatory-course-list.component";
import { MandatoryCourseCardComponent } from "./courses/course-card/mandatory-course-card/mandatory-course-card.component";
import { AuthService } from "../libs/base/services/AuthService";
import { ReactiveFormsModule } from "@angular/forms";
import { NsPermissionsService } from "../libs/base/services/ns-permissions.service";
import { CourseSplashScreenComponent } from "./courses/course-splash-screen/course-splash-screen.component";
import { AppStateService } from "../libs/base/services/AppState.service";
import { CourseActivityList } from "./courses/course-activity-list/course-activity-list.component";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { InfiniteCourseGridComponent } from "./infinite-course-grid/ns-infinite-course-grid.component";
import {InfiniteScrollService} from "../libs/base/services/InfiniteScrollService";
import {MandatoryCourseCardType2Component} from "./courses/course-card/mandatory-course-card-type-2/mandatory-course-card-type2.component";


@NgModule({
  bootstrap: [
    AppComponent
  ],
  imports: [
    CommonModule,
    NativeScriptModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NativeScriptFormsModule,
    NativeScriptUIListViewModule,
  ],
  declarations: [
    AppComponent,
    LoginComponentComponent,
    HomeComponent,
    OptionalCourseListComponent,
    OptionalCourseCardComponent,
    MandatoryCourseListComponent,
    MandatoryCourseCardComponent,
    MandatoryCourseCardType2Component,
    CourseSplashScreenComponent,
    CourseActivityList,
    InfiniteCourseGridComponent,
  ],
  providers: [AuthService, CourseService, NsPermissionsService, AppStateService,InfiniteScrollService],
  schemas: [
    NO_ERRORS_SCHEMA
  ]
})
/*
Pass your application module to the bootstrapModule function located in main.ts to start your app
*/
export class AppModule { }
