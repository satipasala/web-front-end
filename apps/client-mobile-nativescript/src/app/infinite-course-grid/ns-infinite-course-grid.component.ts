import {Component, OnInit, ChangeDetectorRef, Output, EventEmitter, Input} from "@angular/core";
import {ObservableArray} from "tns-core-modules/data/observable-array";
import {
  ListViewLinearLayout,
  RadListView,
  LoadOnDemandListViewEventData,
  ListViewScrollDirection
} from "nativescript-ui-listview";
import {setTimeout} from "tns-core-modules/timer";
import {CourseService} from "~/libs/base/services/CourseService";
import {AbstractDataSourceManager} from "../../libs/core/AbstractDataSourceManager";
import {BehaviorSubject} from "rxjs";
import {InfiniteScrollService} from "../../libs/base/services/InfiniteScrollService";
import {throttleTime} from "rxjs/operators";

@Component({
  moduleId: module.id,
  selector: "ns-infinite-course-grid",
  templateUrl: "ns-infinite-course-grid.component.html",
  styleUrls: ["ns-infinite-course-grid.component.css"]
})

export class InfiniteCourseGridComponent extends AbstractDataSourceManager implements OnInit {

  offset = new BehaviorSubject(null);


  // private layout: ListViewLinearLayout;

  constructor(private _changeDetectionRef: ChangeDetectorRef, protected scrollService: InfiniteScrollService) {
    super(scrollService);
    this.collectionName = "courses"
  }


  ngOnInit(): void {

    /* this.layout = new ListViewLinearLayout();
     this.layout.scrollDirection = ListViewScrollDirection.Horizontal;*/
    this.clearAllFilters();
    //this._changeDetectionRef.detectChanges();

    this.offset.pipe(
      throttleTime(500)
    ).subscribe(value => {
      this.getBatch();

    });
  }


  nextBatch(args: LoadOnDemandListViewEventData) {
    const that = new WeakRef(this);
    const listView: RadListView = args.object;
    if (this.dataSource.reachedEnd() === true) {
      listView.notifyLoadOnDemandFinished(true);
    } else {
      this.offset.next(10);
      setTimeout(()=>{
        listView.notifyLoadOnDemandFinished();
      },1000)
    }
  }
}
