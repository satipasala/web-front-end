import {Component, OnInit} from '@angular/core';
import * as  app from "tns-core-modules/application";
import * as  color from "tns-core-modules/color";
import * as  platformModule from "tns-core-modules/platform";
import * as  orientationModule from "nativescript-screen-orientation";
import {AuthService} from '../../libs/base/services/AuthService';
import {RouterExtensions} from 'nativescript-angular/router';

declare var android: any;

@Component({
  selector: 'ns-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {
  email: string = 'uditha1003@gmail.com';
  password: string = '123456';

  constructor(private authService: AuthService, private router: RouterExtensions) {
  }

  ngOnInit() {

    this.loginpress();
  }

  pageLoaded(args) {
    var page = args.object;
    //orientationModule.setCurrentOrientation("portrait");
    /* custom func. to add OS class & HD/Non-HD class */
    var phone_height = platformModule.screen.mainScreen.heightPixels;
    var isHD = 'no-hd';
    if (phone_height > 1920) {
      isHD = 'hd';
    }
    page.className = page.className + " " + isHD;

    if (platformModule.isIOS) {
      page.className = page.className + " ios";
      args.object.frame.ios.controller.navigationBar.barStyle = 1;
    }

    if (platformModule.isAndroid) {
      page.className = page.className + " android";
    }

    if (platformModule.isAndroid && platformModule.device.sdkVersion >= '23') {
      var View = android.view.View;
      const window = app.android.foregroundActivity.getWindow();
      window.setStatusBarColor(new color.Color("#130D6D").android);
      window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_DARK_STATUS_BAR);
      window.addFlags(android.view.WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
      window.clearFlags(android.view.WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
      window.setNavigationBarColor(new color.Color('#ffffff').android);
    }
    /* custom function end*/

    // page.bindingContext = loginViewModel;
  }

  onNavigatingFrom() {
    orientationModule.orientationCleanup();
  }

  loginpress() {
    this.authService.signInWithEmailAndPassword(this.email, this.password).then(() => {
      this.router.navigate(['/home']);
    }).catch(error => console.log(error));
  }
}
