import { Component, ViewChild } from "@angular/core";
import { AppStateService } from "../../../libs/base/services/AppState.service";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
  selector: 'ns-course-activity-list',
  templateUrl: './course-activity-list.component..html',
  styleUrls: ['./course-activity-list.component..css']
})

export class CourseActivityList {

  @ViewChild("dot-one", {static: false}) d1: any;
  @ViewChild("dot-two", {static: false}) d2: any;
  @ViewChild("slider-wrapper", {static: false}) sliderWrapper: any;

  activities = [];

  constructor(private appState : AppStateService, private router: RouterExtensions){
    this.activities = Object.values(appState.getSelectedCourse().activities);
  }

  status = "not scrolling";
  b_slider = 0;
  c_type = "a";

  barSource = [
    {Day: "Mon", Time: 0,ID:0},
    {Day: "Tue", Time: 0,ID:0},
    {Day: "Wed", Time: 0,ID:0},
    {Day: "Thu", Time: 0,ID:0},
    {Day: "Fri", Time: 0,ID:0},
    {Day: "Sat", Time: 0,ID:0},
    {Day: "Sun", Time: 0,ID:0}
  ];
  dashboardOpen = false;
  dashboard_wrapper_classes = "dashboard-wrapper jump-up";
  ico_class = "ico";
  dash_text = ['This week meditation stats', 'View your weekly stats'];
  dash_ico = "";

  pageLoaded(event : any){}

  pop_msg() {}

  change(){
    this.router.navigate(['/home']);
  }

  showDashboard() {
    if (this.dashboardOpen) {
      this.dash_ico = "";
      this.dashboardOpen = false;
      this.dashboard_wrapper_classes = "dashboard-wrapper";
    }
    else {
      this.dash_ico = "";
      this.dashboardOpen = true;
      this.dashboard_wrapper_classes = "dashboard-wrapper";
      this.barSource = [
        {Day: "Mon", Time: 12, ID: 1},
        {Day: "Tue", Time: 9, ID: 2},
        {Day: "Wed", Time: 14, ID: 3},
        {Day: "Thu", Time: 11, ID: 4},
        {Day: "Fri", Time: 13, ID: 5},
        {Day: "Sat", Time: 15.3, ID: 6},
        {Day: "Sun", Time: 10, ID: 7}
      ];
    }

  }

  onSelect(activity : any){
    console.log("Selected activity : "+activity.name);
  }
}
