import { Component } from '@angular/core';
import { CourseService } from "../../../../libs/base/services/CourseService";
import { Course } from '../../../../../../../libs/base/src/lib/model/Course';

@Component({
  selector: 'ns-optional-course-list',
  templateUrl: './optional-course-list.component.html',
  styleUrls: ['./optional-course-list-component.css']
})

export class OptionalCourseListComponent {

  columnCount: string = "";
  courses: Course[];

  constructor(private _courseService: CourseService) {
    this._courseService.getAll().subscribe(coursesArr => {
      this.courses = coursesArr;
      this.courses.forEach(() => this.columnCount = this.columnCount.concat(" auto"));
    });
  }
}
