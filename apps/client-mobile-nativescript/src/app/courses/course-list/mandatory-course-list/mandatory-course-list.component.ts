import { Component } from '@angular/core';
import { CourseService } from "../../../../libs/base/services/CourseService";
import { Course } from '../../../../../../../libs/base/src/lib/model/Course';
import { AuthService } from '../../../../libs/base/services/AuthService';

@Component({
  selector: 'ns-mandatory-course-list',
  templateUrl: './mandatory-course-list.component.html',
  styleUrls: ['./mandatory-course-list-component.css']
})

export class MandatoryCourseListComponent {

  columnWidth: number = 100;
  courses: Course[] = [];

  constructor( private authService : AuthService, private _courseService: CourseService) {
     this.authService.getCurrentDbUser().subscribe(user => {
      const subscriptionkeys = Object.keys(user.courseSubscriptions);

      subscriptionkeys.forEach(key => {
        this.courses.push(user.courseSubscriptions[key]);
      })

       this.courses.forEach(() => this.columnWidth = this.columnWidth + 100);
    });
  }
}
