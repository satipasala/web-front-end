import {Component, Input} from '@angular/core';
import { AppStateService } from '../../../../libs/base/services/AppState.service';
import { RouterExtensions } from 'nativescript-angular/router';
import {CourseCard} from "../course-card";
@Component({
  selector: 'ns-mandatory-course-card-type-2',
  templateUrl: './mandatory-course-card-type2.component.html',
  styleUrls: ['./mandatory-course-card-type2.component.css']
})

export class MandatoryCourseCardType2Component extends CourseCard   {

  constructor(protected appState : AppStateService,  protected router: RouterExtensions){
    super(appState,router)
  }
}
