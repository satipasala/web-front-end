import {Input} from "@angular/core";
import {Course} from "../../../../../../libs/base/src";
import {AppStateService} from "../../../libs/base/services/AppState.service";
import {RouterExtensions} from "@nativescript/angular";


export abstract class CourseCard {
  @Input() index: number;
  @Input() course : Course;
  protected constructor(protected appState : AppStateService, protected router: RouterExtensions) {
  }

  onSelect() {
    this.appState.setSelectedCourse(this.course);
    this.router.navigate(['/courseSplashScreen']);
  }
}
