import {Component, Input} from '@angular/core';
import { Course } from "../../../../../../../libs/base/src";
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-optional-course-card',
  templateUrl: './optional-course-card.component.html',
  styleUrls: ['./optional-course-card-component.css']
})

export class OptionalCourseCardComponent  {

  @Input() index: number;
  @Input() course : Course;

  imageVersion:number = 0;

  constructor(private router: RouterExtensions){
    this.imageVersion = Math.floor(Math.random() * 3);
  }

  onSelect() {
    this.router.navigate(['/courseGrid']);
  }
}
