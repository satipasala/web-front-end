import {Component, Input} from '@angular/core';
import { AppStateService } from '../../../../libs/base/services/AppState.service';

import {CourseCard} from "../course-card";
import {RouterExtensions} from "@nativescript/angular";
@Component({
  selector: 'ns-mandatory-course-card',
  templateUrl: './mandatory-course-card.component.html',
  styleUrls: ['./mandatory-course-card.component.css']
})

export class MandatoryCourseCardComponent extends CourseCard {

  constructor(protected appState : AppStateService,  protected router: RouterExtensions){
    super(appState,router)
  }

}
