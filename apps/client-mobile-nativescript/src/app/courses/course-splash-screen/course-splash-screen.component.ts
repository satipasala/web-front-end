import {Component, Input} from '@angular/core';
import { Course } from "../../../../../../libs/base/src/lib/model/Course";
import { AppStateService } from '../../../libs/base/services/AppState.service';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-course-splash-screen',
  templateUrl: './course-splash-screen.component.html',
  styleUrls: ['./course-splash-screen.component.css']
})

export class CourseSplashScreenComponent  {

  course : Course;

  constructor(private appState : AppStateService, private router: RouterExtensions){
    this.course = appState.getSelectedCourse();
  }

  beginCourse(){
    this.router.navigate(['/courseActivityList']);
  }

  return(){
    this.router.navigate(['/home']);
  }
}
