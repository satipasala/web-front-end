import { LoginComponentComponent } from './login-component/login-component.component';
import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "@nativescript/angular";
import { Routes } from "@angular/router";
import { HomeComponent } from "~/app/home/home.component";
import { CourseSplashScreenComponent } from './courses/course-splash-screen/course-splash-screen.component';
import { CourseActivityList } from './courses/course-activity-list/course-activity-list.component';
import { InfiniteCourseGridComponent } from './infinite-course-grid/ns-infinite-course-grid.component';


const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },

  { path: "login", component: LoginComponentComponent },
  { path: "home", component: HomeComponent },
  { path: "courseSplashScreen", component: CourseSplashScreenComponent },
  { path: "courseActivityList", component: CourseActivityList },
  {path : "courseGrid", component : InfiniteCourseGridComponent},
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
