import {Component, OnDestroy, OnInit} from "@angular/core";
import { launchEvent, LaunchEventData, on as applicationOn } from "tns-core-modules/application";

import * as firebase from "nativescript-plugin-firebase";
import {Page} from "@nativescript/core";



@Component({
  selector: "ns-app",
  templateUrl: "./app.component.html"
})
export class AppComponent implements OnInit ,OnDestroy{

  constructor() {

  }

  ngOnInit(): void {

    firebase.init({
      persist: false
    }).then(() => {
        console.log('Firebase initiated');
    }).catch(error => console.error(`Error. ${error}`));


    applicationOn(launchEvent, (args: LaunchEventData) => {

      if (args.android) {
        // For Android applications, args.android is an android.content.Intent class.
        console.log("Launched Android application with the following intent: " + args.android + ".");
      } else if (args.ios !== undefined) {
        // For iOS applications, args.ios is NSDictionary (launchOptions).
        console.log("Launched iOS application with options: " + args.ios);
      }
    });
  }

  ngOnDestroy(): void {
  }


}


