import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppShellComponent } from "./components/app-shell/app-shell.component";
import { ResourcesComponent } from './pages/resources/resources.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { EventsComponent } from './pages/events/events.component';
import { SupportUsComponent } from './pages/support-us/support-us.component';
import {CarouselModule  } from "ngx-owl-carousel-o";

import {
  ABOUT_US_ROUTE,
  REGISTRATION_ROUTE,
  EVENTS_ROUTE,
  SUPPORT_US_ROUTE,
  CONTACT_US_ROUTE,
  RESOURCES_ROUTE
} from './app-routs';

const routes: Routes = [
  { path: '', component: AppShellComponent, children: [] },
  { path: RESOURCES_ROUTE, component: ResourcesComponent },
  { path: REGISTRATION_ROUTE, component: RegistrationComponent },
  { path: ABOUT_US_ROUTE, component: AboutUsComponent },
  { path: CONTACT_US_ROUTE, component: ContactUsComponent },
  { path: EVENTS_ROUTE, component: EventsComponent },
  { path: SUPPORT_US_ROUTE, component: SupportUsComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

