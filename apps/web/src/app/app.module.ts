import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialModule } from "./imports/material.module";
import { CommonModule } from "@angular/common";
import {BrowserModule, BrowserTransferStateModule} from "@angular/platform-browser";
import { CoreModule } from "@satipasala/core";
import { AuthGuard, AuthService, BaseModule } from "@satipasala/base";
import { AppShellComponent } from "./components/app-shell/app-shell.component";
import {SearchComponent, UserProfileComponent} from "./components/user-profile/user-profile.component";
import { MenuComponent } from "./components/menu/menu.component";
import { NotificationComponent } from "./components/notification/notification.component";
import { EventBannerComponent } from "./components/event-banner/event-banner.component";
import { EventSliderComponent } from "./components/event-slider/event-slider.component";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ResourcesComponent } from './pages/resources/resources.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { EventsComponent } from './pages/events/events.component';
import { SupportUsComponent } from './pages/support-us/support-us.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { AboutUsHeaderComponent } from './components/about-us/about-us-header/about-us-header.component';
import { AboutUsContentComponent } from './components/about-us/about-us-content/about-us-content.component';
import { ResourcesContentComponent } from './components/resources/resources-content/resources-content.component';
import { ResourcesHeaderComponent } from './components/resources/resources-header/resources-header.component';
import { CloudAnimationComponent } from './components/events/cloud-animation/cloud-animation.component';
import { EventsContentComponent } from './components/events/events-content/events-content.component';
import { VideoContentComponent } from './components/resources/video-content/video-content.component';
import {CarouselModule  } from "ngx-owl-carousel-o";
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {WebLoginComponent} from "./components/user-profile/login/web-login.component";


import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from "@angular/material/core";
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from "@angular/material/form-field";
// IMPORTANT
// Add your own project credentials to environments/*.ts

@NgModule({
  declarations: [AppComponent, AppShellComponent, EventBannerComponent, UserProfileComponent,
    MenuComponent,
    NotificationComponent,
    SearchComponent,
    EventBannerComponent,
    EventSliderComponent,
    ResourcesComponent,
    RegistrationComponent,
    EventsComponent,
    SupportUsComponent,
    AboutUsComponent,
    ContactUsComponent,
    AboutUsHeaderComponent,
    AboutUsContentComponent,
    ResourcesContentComponent,
    ResourcesHeaderComponent,
    CloudAnimationComponent,
    EventsContentComponent,
    VideoContentComponent,
    WebLoginComponent,
  ],

  imports: [
    MaterialModule, //todo remove unwanted material modules here,\
  /*  AngularFirestoreModule.enablePersistence(),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule,*/
    FontAwesomeModule,
    CommonModule,
    BrowserTransferStateModule,
    CoreModule,
    BaseModule,
    AppRoutingModule,
    BrowserModule,
    NgbModule,
    CarouselModule,
    // NgxPageScrollCoreModule,
    BrowserAnimationsModule,
    FormsModule,
    MatDialogModule

    // ServiceWorkerModule.register('/ngsw-worker.js', {
    //   enabled: environment.production
    // })
  ],

  entryComponents: [SearchComponent],
  providers: [AuthService, AuthGuard,
      { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { floatLabel: 'always' } },
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher },
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}

    ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
