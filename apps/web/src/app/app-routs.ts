export const RESOURCES_ROUTE = "resources";
export const REGISTRATION_ROUTE = "registration";
export const EVENTS_ROUTE = "events";
export const SUPPORT_US_ROUTE = "supportus";
export const ABOUT_US_ROUTE = "aboutus";
export const CONTACT_US_ROUTE = "contactus";