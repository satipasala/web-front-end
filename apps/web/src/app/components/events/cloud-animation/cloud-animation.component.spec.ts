import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CloudAnimationComponent } from './cloud-animation.component';

describe('EventsHeaderComponent', () => {
  let component: CloudAnimationComponent;
  let fixture: ComponentFixture<CloudAnimationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CloudAnimationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CloudAnimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
