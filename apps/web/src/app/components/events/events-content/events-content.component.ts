import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'web-events-content',
  templateUrl: './events-content.component.html',
  styleUrls: ['./events-content.component.scss']
})
export class EventsContentComponent implements OnInit {

  eventText = "Sati Pasala was established with the aim of sharing mindfulness with students, teachers and everyone in the school community. \n" +
    "All Mindfulness programs are facilitated by a team of volunteers who are experienced mindfulness practitioners."


  categoryItem = [
    {
      link:  '',
      image: 'assets/event/p_1.svg',
      text: 'Programme',
      class: 'eye_ani',
    },
    {
      link:  '',
      image: 'assets/event/p_2.svg',
      text: 'Library',
      class: ''
    },
    {
      link:  '',
      image: 'assets/event/p_3.svg',
      text: 'Programme',
      class: ''
    },
    {
      link:  '',
      image: 'assets/event/p_4.svg',
      text: 'Library',
      class: 'eye_ani',
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
