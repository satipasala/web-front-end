import {Component, OnInit} from '@angular/core';
import {MatDialog,  MatDialogRef} from '@angular/material/dialog';




@Component({
  selector: 'web-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})


export class UserProfileComponent implements OnInit {


  constructor(public dialog: MatDialog) {}

  openDialog(): void {
    const dialogRef = this.dialog.open(SearchComponent, {
      width: '90%',
      panelClass: 'search-new'

    });

  }

  ngOnInit() {
  }

  openLogin() {

  }
}

@Component({
  selector: 'web-search',
  templateUrl: './search/search.component.html',
  styleUrls: ['./search/search.component.scss']
})


export class SearchComponent {
  constructor(
    public dialogRef: MatDialogRef<SearchComponent> ) {}

}
