import { Component, OnInit } from '@angular/core';
import {AuthService} from "@satipasala/base";

@Component({
  selector: 'web-login-profile',
  templateUrl: './web-login.component.html',
  styleUrls: ['./web-login.component.scss']
})
export class WebLoginComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}
