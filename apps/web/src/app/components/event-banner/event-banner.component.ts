import { Component, OnInit } from '@angular/core';
import {BreakpointOptions, OwlOptions} from 'ngx-owl-carousel-o';
import {} from '@ng-bootstrap/ng-bootstrap'

// var TxtRotate = function (el, toRotate, period) {
//   this.toRotate = toRotate;
//   this.el = el;
//   this.loopNum = 0;
//   this.period = parseInt(period, 10) || 2000;
//   this.txt = '';
//   this.tick();
//   this.isDeleting = false;
// };

// TxtRotate.prototype.tick = function () {
//   var i = this.loopNum % this.toRotate.length;
//   var fullTxt = this.toRotate[i];
//
//   if (this.isDeleting) {
//     this.txt = fullTxt.substring(0, this.txt.length - 1);
//   } else {
//     this.txt = fullTxt.substring(0, this.txt.length + 1);
//   }
//
//   this.el.innerHTML = '<span class="wrap">' + this.txt + '</span>';
//
//   var that = this;
//   var delta = 200 - Math.random() * 100;
//
//   if (this.isDeleting) { delta /= 4; }
//
//   if (!this.isDeleting && this.txt === fullTxt) {
//     delta = this.period;
//     this.isDeleting = true;
//   } else if (this.isDeleting && this.txt === '') {
//     this.isDeleting = false;
//     this.loopNum++;
//     delta = 500;
//   }
//
//   setTimeout(function () {
//     that.tick();
//   }, delta);
// };
//
// window.onload = function () {
//   var elements = document.getElementsByClassName('txt-rotate');
//   for (var i = 0; i < elements.length; i++) {
//     var toRotate = elements[i].getAttribute('data-rotate');
//     var period = elements[i].getAttribute('data-period');
//     if (toRotate) {
//       new TxtRotate(elements[i], JSON.parse(toRotate), period);
//     }
//   }
//   // INJECT CSS
//   var css = document.createElement("style");
//   css.type = "text/css";
//   css.innerHTML = ".txt-rotate > .wrap { border-right: 0.05em solid rgba(255, 255, 255, .2);}";
//   document.body.appendChild(css);
// };

@Component({
  selector: 'event-banner',
  templateUrl: './event-banner.component.html',
  styleUrls: ['./event-banner.component.scss']
})
export class EventBannerComponent implements OnInit {


  constructor() { }
  images = [{
    class:'fade-1',
    src: 'assets/images/1.jpg',
    text: 'Sati Pasala New Year Sati Camp at Bomiriya, Kaduwela'
  }, {
    class:'fade-2',
    src: 'assets/images/02.jpg',
    text: 'Sati Pasala New Year Sati Camp at Bomiriya, Kaduwela',
  },{
   class:'fade-1',
    src: 'assets/images/03.jpg',
    text: 'Sati Pasala New Year Sati Camp at Bomiriya, Kaduwela',
  },{
    class:'fade-2',
    src: 'assets/images/04.jpg',
    text: 'Sati Pasala New Year Sati Camp at Bomiriya, Kaduwela',
  }
  ];
  ngOnInit() {
  }

}
