import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'event-slider',
  templateUrl: './event-slider.component.html',
  styleUrls: ['./event-slider.component.scss']
})
// tslint:disable-next-line:component-class-suffix

export class EventSliderComponent implements OnInit {
  customOptions: OwlOptions = {
    autoWidth:true,
    margin:1,
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    dots: false,
    slideBy: 1,
    autoplay: true,
    autoplayHoverPause: true,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 6
      },
      400: {
        items: 12
      },
      740: {
        items: 12
      },
      940: {
        items: 12
      }
    },
    nav: false
  }
  constructor() {
  }
  imageObject: Array<object> = [
     {
      src: 'assets/images/v-guided-video.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
     },{
      src: 'assets/images/v-mindfulness-summit.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-mindful-sitting.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-mindful-walking.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-minful-brushing.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-minful-eating.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-UN-day-wesak.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/satipasala-mainimage.jpg',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/satipasala-mainimage2.jpg',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/satipasala-mainimage3.jpg',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-minful-brushing.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/v-mindfulness-summit.png',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    },{
      src: 'assets/images/sirasa.jpg',
      link:  '',
      title: 'kanishta-vidyalaya-badulla',
    }
  ];


  ngOnInit() {
  }

}

