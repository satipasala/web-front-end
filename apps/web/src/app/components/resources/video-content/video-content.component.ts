import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'web-video-content',
  templateUrl: './video-content.component.html',
  styleUrls: ['./video-content.component.scss']
})
export class VideoContentComponent implements OnInit {
  sanitizedVideoUrl: SafeResourceUrl;

  @Input() title: string;
  @Input() language: string;
  @Input() imageUrl: string;
  @Input('videoUrl')
  set videoUrl(videoUrl: string) {
    this.sanitizedVideoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
  }

  constructor(private modalService: NgbModal, private sanitizer: DomSanitizer) {
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true, size: 'xl' });
  }

  ngOnInit() {
  }
}
