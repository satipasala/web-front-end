import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcesHeaderComponent } from './resources-header.component';

describe('ResourcesHeaderComponent', () => {
  let component: ResourcesHeaderComponent;
  let fixture: ComponentFixture<ResourcesHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResourcesHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcesHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
