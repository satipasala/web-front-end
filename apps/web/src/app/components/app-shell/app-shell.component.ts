import {Component, OnInit, ViewChild, HostListener, Inject, ElementRef} from '@angular/core';
import {DOCUMENT} from "@angular/common";
@Component({
  selector: 'web-app-shell',
  templateUrl: './app-shell.component.html',
  styleUrls: ['./app-shell.component.scss']
})
export class AppShellComponent implements OnInit {
  screenHeight: any;
  screenWidth: any;


  isScrolling: boolean;
  // isMobile:boolean = false

  constructor(@Inject(DOCUMENT) private document: any,) {
    this.getScreenSize();
  }
  @ViewChild('container', {static: false})
  public container: ElementRef


  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.screenHeight = window.innerHeight + "px";
    this.screenWidth = window.innerWidth + "px";
    // console.log(this.screenHeight, this.screenWidth);
  }



  ngOnInit() {

    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (event): void => {
    const scrollDelta = 100;
    this.isScrolling = (window.scrollY - scrollDelta) > 0;
  };
}
