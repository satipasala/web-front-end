import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router } from "@angular/router";
import {
  ABOUT_US_ROUTE,
  REGISTRATION_ROUTE,
  EVENTS_ROUTE,
  SUPPORT_US_ROUTE,
  CONTACT_US_ROUTE,
  RESOURCES_ROUTE
} from '../../app-routs';

@Component({
  selector: 'web-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None, // disable ViewEncapsulation
})
export class MenuComponent implements OnInit {
  selectedMenuItem = 'Home';
  isNavbarCollapsed = false;
  isScrolling: boolean;
  url: string = this.router.url;

  constructor(@Inject(DOCUMENT) private document: Document, private router: Router) {

  }

  ngOnInit() {
    this.setActivePage();
    window.addEventListener('scroll', this.scroll, true); //third parameter
  }

  ngOnDestroy() {
    window.removeEventListener('scroll', this.scroll, true);
  }

  scroll = (event): void => {
    let scrollDelta = 100;
    if ((window.scrollY - scrollDelta) > 0) {
      this.isScrolling = true;
    } else {
      this.isScrolling = false;
    }
  };


  setActivePage() {
    this.mainNav.forEach(item => {
      if (this.url.substring(1) === item.routerLink) {
        this.selectedMenuItem = item.headingName;
      }
    })
  }


  mainNav = [
    { headingName: 'Home', routerLink: "" },
    { headingName: 'Resources', routerLink: RESOURCES_ROUTE },
    { headingName: 'Registration', routerLink: REGISTRATION_ROUTE },
    { headingName: 'Events', routerLink: EVENTS_ROUTE },
    { headingName: 'Support Us', routerLink: SUPPORT_US_ROUTE },
    { headingName: 'About Us', routerLink: ABOUT_US_ROUTE },
    { headingName: 'contact Us', routerLink: CONTACT_US_ROUTE },
  ];


  onMenuItemClicked(menuItem: mainNav) {
    console.log('clicked ' + menuItem);
    this.selectedMenuItem = menuItem.headingName;
  }
}


export class mainNav {
  headingName: string;
  icon: string;
  dropDown: boolean;
  // subCategory: subNavItem[];
  headingLink: string;
  isNavbarCollapsed: boolean = true;

  constructor(_categoryName: string, _icon: string, _dropDown: boolean, categoryLink: string = "") {
    this.headingName = _categoryName;
    this.icon = _icon;
    this.dropDown = _dropDown;
    // this.subCategory = _subCategory;
    this.headingLink = categoryLink;

  }
}
