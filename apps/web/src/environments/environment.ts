// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDZ-Y3B25JTvQQ-C_eHoqE6-Lf93eMz_fg",
    authDomain: "satipasala-dev-cc45b.firebaseapp.com",
    databaseURL: "https://satipasala-dev-cc45b.firebaseio.com",
    projectId: "satipasala-dev-cc45b",
    storageBucket: "satipasala-dev-cc45b.appspot.com",
    messagingSenderId: "405174482203"
  },
  functions: {
    resetUserPasswordUrl : 'https://us-central1-sati-pasala-lc-dev.cloudfunctions.net/authUserOnResetPassword'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
