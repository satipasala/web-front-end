import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "../../imports/material.module";
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { MenuComponent } from './components/menu/menu.component';
import { NotificationComponent } from './components/notification/notification.component';
import {AngularFirestore} from "@angular/fire/firestore";
import {CoreModule} from "@satipasala/core";
import {AuthService, BaseModule} from "@satipasala/base";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatSelectModule} from "@angular/material/select";
import {MatRadioModule} from "@angular/material/radio";
import {MatCardModule} from "@angular/material/card";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatMenuModule} from "@angular/material/menu";
import {MatSortModule} from "@angular/material/sort";
import {MatIconModule} from "@angular/material/icon";

@NgModule({
  providers: [AngularFirestore,AuthService],
  declarations: [
  UserProfileComponent,
  MenuComponent,
  NotificationComponent,
 ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    ReactiveFormsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatMenuModule,
    MatIconModule,
    MaterialModule,
    CoreModule,
    BaseModule
  ],

  exports: [
    UserProfileComponent,
    MenuComponent,
    NotificationComponent
  ]
})
export class CoreClientModule {
}
