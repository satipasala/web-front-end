import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ClientAppShellComponent} from "./app-shell.component/client-app-shell.component";


const routes: Routes = [
  {
    path: '', component: ClientAppShellComponent,
    children: [

    ]
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppShellRoutingModule {
}
