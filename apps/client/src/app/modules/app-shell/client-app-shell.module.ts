import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppShellRoutingModule } from './app-shell-routing.module';
import {MaterialModule} from "../../imports/material.module";
import {AngularFirestoreModule} from "@angular/fire/firestore";
import {AngularFireStorageModule} from "@angular/fire/storage";
import {AngularFireFunctionsModule} from "@angular/fire/functions";
import {BrowserTransferStateModule} from "@angular/platform-browser";
import 'hammerjs';
import {AuthGuard, AuthService, BaseModule } from "@satipasala/base";
import {CoreClientModule} from "../core/core-client.module";
import {CoreModule} from "@satipasala/core";
import {ErrorStateMatcher, ShowOnDirtyErrorStateMatcher} from "@angular/material/core";
import {ClientAppShellComponent} from "./app-shell.component/client-app-shell.component";
import {MAT_FORM_FIELD_DEFAULT_OPTIONS} from "@angular/material/form-field";

@NgModule({
  declarations: [ClientAppShellComponent],
  imports: [
    MaterialModule, //todo remove unwanted material modules here,\
  /*  AngularFirestoreModule.enablePersistence(),*/
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireFunctionsModule,
    CommonModule,
    CoreClientModule,
    BrowserTransferStateModule,
    AppShellRoutingModule,
    BaseModule,
    CoreModule
  ],
  providers: [AuthService,AuthGuard,
    {provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: {floatLabel: 'always'}},
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}]

})
export class ClientAppShellModule { }
