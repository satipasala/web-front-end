import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ClientAppShellComponent} from "./client-app-shell.component";



describe('AppShellComponent', () => {
  let component: ClientAppShellComponent;
  let fixture: ComponentFixture<ClientAppShellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientAppShellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientAppShellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
