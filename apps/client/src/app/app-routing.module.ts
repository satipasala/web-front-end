import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {path: '',  loadChildren: () => import('./modules/app-shell/client-app-shell.module').then(m => m.ClientAppShellModule)   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }

