import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoleViewCardComponent } from './role-view-card.component';

describe('RoleViewCardComponent', () => {
  let component: RoleViewCardComponent;
  let fixture: ComponentFixture<RoleViewCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoleViewCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoleViewCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
