import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventHistogramChart } from './event-histogram-chart.component';

describe('DashboardCardComponentComponent', () => {
  let component: EventHistogramChart;
  let fixture: ComponentFixture<EventHistogramChart>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventHistogramChart ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventHistogramChart);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
