import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TranslationPage} from './translation-page.component';

describe('TranslationPageComponent', () => {
  let component: TranslationPage;
  let fixture: ComponentFixture<TranslationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TranslationPage]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranslationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
