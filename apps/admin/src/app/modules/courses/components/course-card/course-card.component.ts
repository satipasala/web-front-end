import { Component, Input, OnInit } from '@angular/core';
import { Course, CoursesService } from "@satipasala/base";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'admin-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent implements OnInit {

  @Input()
  course: Course;

  rolePermission: Object;

  @Input()
  set permission(permission: Object) {
    this.rolePermission = permission;
  }

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
  }

  editCourse(course: Course) {
    this.router.navigate([{ outlets: { leftsidebar: [course.id, "edit"] } }], { relativeTo: this.activatedRoute.parent });
  }

  showActivites(course: Course) {
    this.router.navigate([{ outlets: { leftsidebar: ["activities", course.id] } }], { relativeTo: this.activatedRoute.parent });
  }

  addStudents(course: Course) {
    this.router.navigate([{ outlets: { leftsidebar: ["course", "assign", course.id] } }], { relativeTo: this.activatedRoute.parent });
  }

  getActivityCount(course: Course):number {
    return Object.keys(course?.activities).length
  }
}
