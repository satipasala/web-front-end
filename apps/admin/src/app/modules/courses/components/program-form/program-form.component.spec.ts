import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramFormComponent } from './program-form.component';
import {CoreModule, DynamicFormComponent, ErrorStateMatcherFactory} from "@satipasala/core";


describe('CourseFormComponent', () => {
  let component: ProgramFormComponent;
  let fixture: ComponentFixture<ProgramFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers:[ErrorStateMatcherFactory],
      declarations: [ ProgramFormComponent,DynamicFormComponent ],
      imports:[CoreModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
