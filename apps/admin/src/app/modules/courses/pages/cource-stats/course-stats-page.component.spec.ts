import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {CourseStatsPage} from "./course-stats-page.component";


describe('CourseStatsPage', () => {
  let component: CourseStatsPage;
  let fixture: ComponentFixture<CourseStatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseStatsPage ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseStatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
