export const DEFAULT_USER_IMAGE = 'assets/images/img_avatar2.png';
export const USER_IMAGE_FOLDER = "profile_pictures/";
export const FIRABASE_STORAGE = "firebasestorage";
export const LOGIN_IN_PROGRESS_KEY = "SatiLoginInProgress";
