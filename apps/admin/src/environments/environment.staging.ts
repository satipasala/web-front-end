export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyCg_rkr14yT48iyU0GujPcZnzAy7SX0mvY",
    authDomain: "satipasala-staging.firebaseapp.com",
    databaseURL: "https://satipasala-staging.firebaseio.com",
    projectId: "satipasala-staging",
    storageBucket: "satipasala-staging.appspot.com",
    messagingSenderId: "373744509383",
    appId: "1:373744509383:web:0f5ad46002a17f6db939e2"
  },
  functions: {
    resetUserPasswordUrl : 'https://us-central1-satipasala-dev-cc45b.cloudfunctions.net/resetAuthUserPassword',
  },
  googleCharts:{
    mapsApiKey:'AIzaSyDZ-Y3B25JTvQQ-C_eHoqE6-Lf93eMz_fg'
  }
};
