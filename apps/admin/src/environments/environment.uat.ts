export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyD_RrOiIMTYR7maB1Kplnt-8OXHshtaSiA",
    authDomain: "satipasala-uat.firebaseapp.com",
    databaseURL: "https://satipasala-uat.firebaseio.com",
    projectId: "satipasala-uat",
    storageBucket: "satipasala-uat.appspot.com",
    messagingSenderId: "410428102",
  appId: "1:410428102:web:a3c7038c2396d6857dadc7"
  },
  functions: {
    resetUserPasswordUrl : 'api/resetAuthUserPassword',
  },
  googleCharts:{
    mapsApiKey:'AIzaSyDZ-Y3B25JTvQQ-C_eHoqE6-Lf93eMz_fg'
  }
};
