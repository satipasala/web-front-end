export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAO7vdCeW-TX0LE14jI9LnI5AYsXfftjsg",
    authDomain: "satipasala-production.firebaseapp.com",
    databaseURL: "https://satipasala-production.firebaseio.com",
    projectId: "satipasala-production",
    storageBucket: "satipasala-production.appspot.com",
    messagingSenderId: "286311469392",
    appId: "1:286311469392:web:cbf003d0906aff00c11c55"
  },
  functions: {
    resetUserPasswordUrl : 'api/resetAuthUserPassword',
  },
  googleCharts:{
    mapsApiKey:'AIzaSyDZ-Y3B25JTvQQ-C_eHoqE6-Lf93eMz_fg'
  }
};
