import 'package:flutter/material.dart';
import 'package:satipasalaappv2/Firebase/CollectionService.dart';
import 'package:satipasalaappv2/Firebase/FirebaseDataSource.dart';
import 'EventSession/EventSessionCard.dart';

import '../../Styles/AppStyles.dart';
import '../../../Firebase/model/EventSession.dart';

class EventFeed extends StatefulWidget {
  @override
  _EventFeedState createState() => _EventFeedState();
}

class _EventFeedState extends State<EventFeed> {
  num _pageSize = 10;
  ScrollController _scrollController;
  FirebaseDataSource _firebaseDataSource;

  @override
  void initState() {
    _firebaseDataSource = FirebaseDataSource<EventSession>(
        _pageSize,
        CollectionService(
            "eventSessions",
            (Map<String, dynamic> snapshot, [String id]) =>
                EventSession.fromSnapshot(snapshot, id)));
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _firebaseDataSource.nextBatch();

    super.initState();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      _firebaseDataSource
          .nextBatch(); //todo fix this next batch call since its not efficient.
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top";

    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Upcoming Events",
              style: headerTextStyle,
            ),
            // InkWell(
            //     onTap: () async {},
            //     child: Text(
            //       "See All",
            //       style: TextStyle(color: Color(0xff7583CA), fontSize: 20),
            //     ))
          ],
        ),
        SizedBox(
          height: 15,
        ),
        Container(
            margin: EdgeInsets.all(5.0),
            height: 150,
            width: 333.0,
            child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                controller: _scrollController,
                //children: [EventCard(), EventCard(), EventCard()],
                children: [
                  StreamBuilder<List<EventSession>>(
                      stream: _firebaseDataSource.connect(),
                      builder: (context, snapshot) {
                        if (!snapshot.hasData) {
                          return Center(
                            child: CircularProgressIndicator(),
                          );
                        } else {
                          return buildList(snapshot);
                        }
                      })
                ])),
      ],
    ));
  }

  Widget buildList(AsyncSnapshot<List<EventSession>> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: snapshot.data.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return EventSessionCard(snapshot.data[index]);
        });
  }
}
