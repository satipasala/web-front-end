import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:satipasalaappv2/Firebase/model/Course.dart';
import 'package:satipasalaappv2/Firebase/model/EventSession.dart';
import 'package:satipasalaappv2/UI/Components/Courses/CourseCard/CourseCard.dart';
import '../../../Styles/AppStyles.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../../Camera/camera.dart';

class EventSessionDetails extends StatefulWidget {
  final EventSession event;
  const EventSessionDetails(this.event);
  @override
  _EventSessionDetailsState createState() => _EventSessionDetailsState();
}

class _EventSessionDetailsState extends State<EventSessionDetails> {
  Map<dynamic, dynamic> program;
  List<CourseInfo> courseInfo;

  @override
  void initState() {
    // TODO: implement initState
    program = HashMap.from(widget.event.program);
    var courses = new Map<String, dynamic>.from(program['courses']);
    courseInfo =
        courses.entries.map((e) => CourseInfo.fromSnapshot(e.value)).toList();
    super.initState();
  }

  Widget buildCourseCards() {
    return GridView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: courseInfo.length,
        physics: NeverScrollableScrollPhysics(),
        gridDelegate:
            SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (BuildContext context, int index) {
          return GridTile(child: CourseCard(courseInfo[index]));
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff03174C),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Stack(
              children: [
                Image.asset('assets/sati/image04.jpg'),
                Positioned(
                  top: 40.0,
                  left: 20.0,
                  child: InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.black45,
                          size: 30,
                        )),
                  ),
                ),
                // Positioned(
                //     top: 160,
                //     left: MediaQuery.of(context).size.width * 0.8,
                //     child: InkWell(
                //       onTap: () {
                //         showCupertinoModalBottomSheet(
                //           context: context,
                //           builder: (context) => Container(
                //             height: 200,
                //             child: Row(
                //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                //               children: [
                //                 Column(
                //                   mainAxisAlignment: MainAxisAlignment.center,
                //                   children: [
                //                     GestureDetector(
                //                       onTap: () {
                //                         Navigator.push(
                //                           context,
                //                           MaterialPageRoute(
                //                               builder: (context) =>
                //                                   CaptureImage()),
                //                         );
                //                       },
                //                       child: Container(
                //                         child: Column(
                //                           children: [
                //                             Icon(
                //                               Icons.camera_alt,
                //                               color: Colors.black45,
                //                               size: 80,
                //                             ),
                //                             Text(
                //                               "Camera",
                //                               style: TextStyle(
                //                                   fontSize: 12,
                //                                   color: Colors.black38),
                //                             )
                //                           ],
                //                         ),
                //                       ),
                //                     ),
                //                   ],
                //                 ),
                //                 Column(
                //                   mainAxisAlignment: MainAxisAlignment.center,
                //                   children: [
                //                     Icon(
                //                       Icons.image,
                //                       color: Colors.black45,
                //                       size: 80,
                //                     ),
                //                     Text(
                //                       "Choose from gallery",
                //                       style: TextStyle(
                //                           fontSize: 12, color: Colors.black38),
                //                     )
                //                   ],
                //                 )
                //               ],
                //             ),
                //           ),
                //         );
                //       },
                //       child: Container(
                //           decoration: BoxDecoration(
                //             color: Colors.white,
                //             shape: BoxShape.circle,
                //           ),
                //           child: Icon(
                //             Icons.camera,
                //             color: Colors.black45,
                //             size: 50,
                //           )),
                //     ))
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Session",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.amberAccent,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        width: MediaQuery.of(context).size.width * 0.9,
                        child: Text(
                          widget.event.name,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.normal,
                              fontSize: 20),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "Session Start Date : " + widget.event.startDate,
                        style: TextStyle(color: Colors.white70),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Courses", style: subHeadingTextStyle),
                    ],
                  ),
                  Container(
                      margin: const EdgeInsets.only(top: 5, bottom: 10),
                      width: MediaQuery.of(context).size.width * 0.95,
                      child: buildCourseCards())
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
