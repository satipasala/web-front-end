import 'package:flutter/material.dart';
import 'package:satipasalaappv2/Firebase/model/EventSession.dart';
import '../../../Styles/AppStyles.dart';
import 'EventSessionDetails.dart';

class EventSessionCard extends StatefulWidget {
  final EventSession event;
  EventSessionCard(this.event);

  @override
  _EventSessionCardState createState() => _EventSessionCardState();
}

class _EventSessionCardState extends State<EventSessionCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(right: 25),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => EventSessionDetails(widget.event)),
              );
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(5.0),
              child: Stack(
                children: [
                  Image.asset(
                    "assets/medi02.jpeg",
                    height: 100.0,
                    width: 140.0,
                    fit: BoxFit.cover,
                  ),
                  widget.event.status['value'].toString().toLowerCase() ==
                          "started"
                      ? Positioned(
                          top: 6,
                          left: 6,
                          child: Icon(
                            Icons.circle,
                            color: Colors.green,
                            size: 15,
                          ))
                      : Container()
                ],
              ),
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Container(
            width: 130,
            child: Text(
              widget.event.name,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(color: textColor, fontWeight: FontWeight.w400),
            ),
          )
          //     ClipRRect(
          //   borderRadius: BorderRadius.circular(5.0),
          //   child: Stack(
          //     children: <Widget>[
          //       ColorFiltered(
          //         colorFilter: ColorFilter.mode(
          //             Colors.black.withOpacity(0.25), BlendMode.darken),
          //         child: Image.asset(
          //           "assets/medi02.jpeg",
          //           height: 100.0,
          //           width: 140.0,
          //           fit: BoxFit.cover,
          //         ),
          //       ),
          //       Positioned(
          //         child: Container(
          //           width: 130,
          //           child: Text(
          //             event.name,
          //             maxLines: 2,
          //             overflow: TextOverflow.ellipsis,
          //             style: TextStyle(
          //                 color: textColor, fontWeight: FontWeight.w700),
          //           ),
          //         ),
          //         top: 15,
          //       ),
          //     ],
          //   ),
          // ),
          // )
        ],
      ),
    );
  }
}
