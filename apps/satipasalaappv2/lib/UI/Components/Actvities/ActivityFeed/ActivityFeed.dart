import 'package:flutter/material.dart';
import 'package:satipasalaappv2/UI/Components/Sati/ActivityFeed/SatiActivityCard.dart';
import '../ActivityCard/ActivityCard.dart';
import '../../../../Firebase/model/Activity.dart';
import '../../../../Firebase/FirebaseDataSource.dart';
import '../../../../Firebase/CollectionService.dart';

class ActivtyFeed extends StatefulWidget {
  @override
  _ActivtyFeedState createState() => _ActivtyFeedState();
}

class _ActivtyFeedState extends State<ActivtyFeed> {
  num _pageSize = 10;
  ScrollController _scrollController;
  FirebaseDataSource _firebaseDataSource;
  @override
  void initState() {
    _firebaseDataSource = FirebaseDataSource<Activity>(
        _pageSize,
        CollectionService(
            "activities",
            (Map<String, dynamic> snapshot, [String id]) =>
                Activity.fromSnapshot(snapshot, id)));
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _firebaseDataSource.nextBatch();
    super.initState();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      _firebaseDataSource
          .nextBatch(); //todo fix this next batch call since its not efficient.
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top";

    }
  }

  Widget buildList(AsyncSnapshot<List<Activity>> snapshot) {
    return GridView.builder(
        scrollDirection: Axis.vertical,
        itemCount: snapshot.data.length,
        controller: _scrollController,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 0.8,
          crossAxisSpacing: 1,
          crossAxisCount: 2,
        ),
        itemBuilder: (BuildContext context, int index) {
          return SatiActivityCard(snapshot.data[index]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: StreamBuilder<List<Activity>>(
            stream: _firebaseDataSource.connect(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return buildList(snapshot);
              }
            }));
  }
}
