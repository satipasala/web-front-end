import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class PDF extends StatefulWidget {
  final String link;
  final String name;
  const PDF(this.link, this.name);
  @override
  _PDFState createState() => _PDFState();
}

class _PDFState extends State<PDF> {
  bool _isLoading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async => false,
        child: new MaterialApp(
          routes: {
            "/": (_) => new WebviewScaffold(
                  url: widget.link,
                  appBar: new AppBar(
                    leading: new IconButton(
                      icon: new Icon(Icons.arrow_back),
                      onPressed: () => Navigator.of(context).pop(),
                    ),
                    centerTitle: true,
                    backgroundColor: Color(0xff03174C),
                    title: new Text(widget.name),
                  ),
                  initialChild: Container(
                    color: Colors.white54,
                    child: const Center(
                      child: Text('Waiting.....'),
                    ),
                  ),
                )
          },
        ));
  }
}
