import 'package:flutter/material.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:flutter_isolate/flutter_isolate.dart';
import 'dart:isolate';
import 'dart:io'; // for exit();

class ElapsedTime {
  final int hundreds;
  final int seconds;
  final int minutes;

  ElapsedTime({
    this.hundreds,
    this.seconds,
    this.minutes,
  });
}

class Dependencies {
  final List<ValueChanged<ElapsedTime>> timerListeners =
      <ValueChanged<ElapsedTime>>[];
  final TextStyle textStyle = const TextStyle(
      color: Colors.white, fontSize: 60.0, fontFamily: "Bebas Neue");
  final Stopwatch stopwatch = new Stopwatch();
  final int timerMillisecondsRefreshRate = 30;
}

class TimerPage extends StatefulWidget {
  final String uid;
  const TimerPage({
    Key key,
    @required this.uid,
  }) : super(key: key);

  TimerPageState createState() => new TimerPageState();
}

class TimerPageState extends State<TimerPage> {
  final Dependencies dependencies = new Dependencies();
  FirebaseFirestore dbInstance = FirebaseFirestore.instance;
  ProgressDialog pr;
  bool resetBtn = false;
  bool saveBtn = false;
  bool startBtn = false;
  bool stopBtn = false;
  bool loading = false;
  String textHint = "Select Type";
  bool textHintStatus = false;
  Isolate isolate;

  void leftButtonPressed() {
    setState(() {
      saveBtn = false;
      resetBtn = true;
      dependencies.stopwatch.reset();
    });
  }

  void saveButtonPressed() {
    setState(() {
      saveBtn = false;
      loading = true;
    });

    pr.show();
    print(widget.uid);
    dbInstance
        .collection('Users')
        .doc(widget.uid)
        .collection('Timings')
        .doc()
        .set({
      'type': textHint,
      'totalTime': dependencies.stopwatch.elapsedMilliseconds,
      'date': DateTime.now()
    }).then((value) {
      dependencies.stopwatch.reset();
      pr.hide();
      setState(() {
        loading = false;
      });
    });
  }

  void rightButtonPressed() async {
    setState(() {
      if (dependencies.stopwatch.isRunning) {
        stopBtn = true;
        saveBtn = true;
        dependencies.stopwatch.stop();
      } else {
        if (textHint.toLowerCase().contains("select")) {
          textHintStatus = true;
        } else {
          dependencies.stopwatch.start();
          startBtn = true;
          stopBtn = false;
          textHintStatus = false;
        }
      }
    });
  }

  Widget buildFloatingButton(String text, VoidCallback callback) {
    TextStyle roundTextStyle =
        const TextStyle(fontSize: 16.0, color: Colors.white);
    return new FloatingActionButton(
        backgroundColor:
            text.toLowerCase() == "reset" ? Colors.green : Colors.redAccent,
        child: new Text(text, style: roundTextStyle),
        onPressed: callback);
  }

  Widget buildSaveFloatingButton(String text, VoidCallback callback) {
    TextStyle roundTextStyle =
        const TextStyle(fontSize: 16.0, color: Colors.white);
    return new FloatingActionButton(
        backgroundColor: Colors.lightBlueAccent,
        child: new Text(text, style: roundTextStyle),
        onPressed: callback);
  }

  Widget dropDown() {
    return new DropdownButton<String>(
      style: TextStyle(
          fontSize: 15, color: Colors.black54, fontFamily: "Montserrat-Medium"),
      hint: Text(
        textHint,
        style:
            TextStyle(color: textHintStatus ? Colors.redAccent : Colors.white),
      ),
      items: <String>[
        'Mindful Sitting',
        'Mindful Listening',
        'Mindful Eating',
        'Mindful Walking'
      ].map((String value) {
        return new DropdownMenuItem<String>(
          value: value,
          child: new Text(value),
        );
      }).toList(),
      onChanged: (_value) {
        setState(() {
          textHint = _value;
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    pr = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: false);
    pr.style(
        message: 'Saving progress...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: CircularProgressIndicator(),
        elevation: 10.0,
        insetAnimCurve: Curves.easeInOut,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: Colors.black54, fontSize: 13.0, fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: Colors.black54,
            fontSize: 19.0,
            fontWeight: FontWeight.w600));

    return Container(
      color: Color(0xff819be3),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.4,
            width: MediaQuery.of(context).size.width * 0.6,
            child: new Image.asset("assets/meditate_anim.gif"),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 30),
          ),
          TimerText(dependencies: dependencies),
          new Padding(
            padding: const EdgeInsets.only(top: 10),
            child: new Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                buildFloatingButton("reset", leftButtonPressed),
                saveBtn
                    ? buildSaveFloatingButton("save", saveButtonPressed)
                    : dependencies.stopwatch.isRunning
                        ? Container()
                        : dropDown(),
                buildFloatingButton(
                    dependencies.stopwatch.isRunning ? "stop" : "start",
                    rightButtonPressed),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TimerText extends StatefulWidget {
  TimerText({this.dependencies});
  final Dependencies dependencies;

  TimerTextState createState() =>
      new TimerTextState(dependencies: dependencies);
}

class TimerTextState extends State<TimerText> {
  TimerTextState({this.dependencies});
  final Dependencies dependencies;
  Timer timer;
  int milliseconds;

  @override
  void initState() {
    timer = new Timer.periodic(
        new Duration(milliseconds: dependencies.timerMillisecondsRefreshRate),
        callback);
    super.initState();
  }

  @override
  void dispose() {
    timer?.cancel();
    timer = null;
    super.dispose();
  }

  void callback(Timer timer) {
    if (milliseconds != dependencies.stopwatch.elapsedMilliseconds) {
      milliseconds = dependencies.stopwatch.elapsedMilliseconds;
      final int hundreds = (milliseconds / 10).truncate();
      final int seconds = (hundreds / 100).truncate();
      final int minutes = (seconds / 60).truncate();
      final ElapsedTime elapsedTime = new ElapsedTime(
        hundreds: hundreds,
        seconds: seconds,
        minutes: minutes,
      );
      for (final listener in dependencies.timerListeners) {
        listener(elapsedTime);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        new RepaintBoundary(
          child: new SizedBox(
            height: 100.0,
            child: new MinutesAndSeconds(dependencies: dependencies),
          ),
        ),
        new RepaintBoundary(
          child: new SizedBox(
            height: 100.0,
            child: new Hundreds(dependencies: dependencies),
          ),
        ),
      ],
    );
  }
}

class MinutesAndSeconds extends StatefulWidget {
  MinutesAndSeconds({this.dependencies});
  final Dependencies dependencies;

  MinutesAndSecondsState createState() =>
      new MinutesAndSecondsState(dependencies: dependencies);
}

class MinutesAndSecondsState extends State<MinutesAndSeconds> {
  MinutesAndSecondsState({this.dependencies});
  final Dependencies dependencies;

  int minutes = 0;
  int seconds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.minutes != minutes || elapsed.seconds != seconds) {
      setState(() {
        minutes = elapsed.minutes;
        seconds = elapsed.seconds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');
    return new Text('$minutesStr:$secondsStr.', style: dependencies.textStyle);
  }
}

class Hundreds extends StatefulWidget {
  Hundreds({this.dependencies});
  final Dependencies dependencies;

  HundredsState createState() => new HundredsState(dependencies: dependencies);
}

class HundredsState extends State<Hundreds> {
  HundredsState({this.dependencies});
  final Dependencies dependencies;

  int hundreds = 0;

  @override
  void initState() {
    dependencies.timerListeners.add(onTick);
    super.initState();
  }

  void onTick(ElapsedTime elapsed) {
    if (elapsed.hundreds != hundreds) {
      setState(() {
        hundreds = elapsed.hundreds;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String hundredsStr = (hundreds % 100).toString().padLeft(2, '0');
    return new Text(hundredsStr, style: dependencies.textStyle);
  }
}
