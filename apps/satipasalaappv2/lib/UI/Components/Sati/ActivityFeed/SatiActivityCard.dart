import 'package:flutter/material.dart';
import '../../../../Firebase/model/Activity.dart';
import '../../Actvities/ActivityLauncher/PDF.dart';
import '../../Actvities/ActivityLauncher/Meditate/dashboard.dart';
import '../../Actvities/ActivityLauncher/Audio/Audio.dart';
import '../../Actvities/ActivityLauncher/Video.dart';
import 'dart:collection';

class SatiActivityCard extends StatelessWidget {
  Activity activity;
  SatiActivityCard(this.activity);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        var link = activity.resource['link'];
        var name = activity.name;

        var type = activity.type.contentType['type'];

        var widget;
        try {
          if (type == "pdf") {
            widget = PDF(link, name);
          } else if (type == "audio") {
            widget = Video(link);
          } else if (type == "meditate") {
            widget = MeditateDashbaord(
              uid: "",
            );
          } else if (type == "video") {
            widget = Video(link);
          } else {
            throw ("Invalid Activity");
          }
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => widget),
          );
        } catch (ex) {
          print(ex);
        }
      },
      child: Container(
        padding: const EdgeInsets.only(
          right: 20,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: Container(
                  color: Color(0xff84C6AE),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(5.0),
                    child: Image.asset(
                      "assets/medi01.jpeg",
                      height: MediaQuery.of(context).size.height * 0.2,
                      width: MediaQuery.of(context).size.width * 0.4,
                      fit: BoxFit.cover,
                    ),
                  ),
                )),
            SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Text(activity.name,
                  style: TextStyle(color: Colors.white, fontSize: 15)),
            ),
            Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5, top: 5),
                  child: Text(activity.type.name,
                      style: TextStyle(
                          color: Colors.white70,
                          fontSize: 13,
                          fontStyle: FontStyle.italic)),
                ),
                // Padding(
                //   padding: const EdgeInsets.only(left: 5),
                //   child: Text("30-10 mins",
                //       style: TextStyle(color: Colors.white70, fontSize: 14)),
                // ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
