import 'package:flutter/material.dart';
import 'SatiActivityCard.dart';
import '../../../../Firebase/model/Activity.dart';
import '../../../../Firebase/FirebaseDataSource.dart';
import '../../../../Firebase/CollectionService.dart';

class SatiActivityFeed extends StatefulWidget {
  @override
  _SatiActivityFeedState createState() => _SatiActivityFeedState();
}

class _SatiActivityFeedState extends State<SatiActivityFeed> {
  num _pageSize = 10;
  ScrollController _scrollController;
  FirebaseDataSource _firebaseDataSource;
  @override
  void initState() {
    _firebaseDataSource = FirebaseDataSource<Activity>(
        _pageSize,
        CollectionService(
            "activities",
            (Map<String, dynamic> snapshot, [String id]) =>
                Activity.fromSnapshot(snapshot, id)));
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _firebaseDataSource.nextBatch();
    super.initState();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      _firebaseDataSource
          .nextBatch(); //todo fix this next batch call since its not efficient.
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top";

    }
  }

  Widget buildList(AsyncSnapshot<List<Activity>> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: snapshot.data.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return SatiActivityCard(snapshot.data[index]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          margin: EdgeInsets.all(5.0),
          height: 300,
          width: 333.0,
          child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              controller: _scrollController,
              children: [
                StreamBuilder<List<Activity>>(
                    stream: _firebaseDataSource.connect(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return buildList(snapshot);
                      }
                    })
              ]),
        ));
  }
}
