import 'package:flutter/material.dart';
import 'CourseCard.dart';
import '../../../../Firebase/model/Course.dart';
import '../../../../Firebase/FirebaseDataSource.dart';
import '../../../../Firebase/CollectionService.dart';

class SatiCourseFeed extends StatefulWidget {
  @override
  _SatiCourseFeedState createState() => _SatiCourseFeedState();
}

class _SatiCourseFeedState extends State<SatiCourseFeed> {
  num _pageSize = 2;
  ScrollController _scrollController;
  FirebaseDataSource _firebaseDataSource;
  @override
  void initState() {
    _firebaseDataSource = FirebaseDataSource<CourseInfo>(
        _pageSize,
        CollectionService(
            "courses",
            (Map<String, dynamic> snapshot, [String id]) =>
                CourseInfo.fromSnapshot(snapshot, id)));
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _firebaseDataSource.nextBatch();
    super.initState();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      _firebaseDataSource
          .nextBatch(); //todo fix this next batch call since its not efficient.
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top";

    }
  }

  Widget buildList(AsyncSnapshot<List<CourseInfo>> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemCount: snapshot.data.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return SatiCourseCard(snapshot.data[index]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          margin: EdgeInsets.all(5.0),
          height: 200,
          width: 333.0,
          child: ListView(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              controller: _scrollController,
              children: [
                StreamBuilder<List<CourseInfo>>(
                    stream: _firebaseDataSource.connect(),
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      } else {
                        return buildList(snapshot);
                      }
                    })
              ]),
        ));
  }
}
