import 'package:flutter/material.dart';
import 'package:satipasalaappv2/Firebase/model/User.dart';
import 'package:satipasalaappv2/Firebase/model/base/FirestoreDocument.dart';
import 'ActivityCard.dart';
import 'package:satipasalaappv2/Firebase/CollectionService.dart';
import 'package:satipasalaappv2/Firebase/FirebaseDataSource.dart';
import '../../../../Firebase/model/Action.dart';
import '../../User/UserActions/UserActionCard.dart';

class ActivityFeed extends StatefulWidget {
  @override
  _ActivityFeedState createState() => _ActivityFeedState();
}

class _ActivityFeedState extends State<ActivityFeed> {
  num _pageSize = 10;
  ScrollController _scrollController;
  FirebaseDataSource _firebaseDataSource;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _firebaseDataSource = FirebaseDataSource<UserActions>(
        _pageSize,
        CollectionService(
            "actionFeed",
            (Map<String, dynamic> snapshot, [String id]) =>
                UserActions.fromSnapshot(snapshot, id)));
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _firebaseDataSource.nextBatch();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      _firebaseDataSource
          .nextBatch(); //todo fix this next batch call since its not efficient.
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top";

    }
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            controller: _scrollController,
            //children: [EventCard(), EventCard(), EventCard()],
            children: [
              StreamBuilder<List<UserActions>>(
                  stream: _firebaseDataSource.connect(),
                  builder: (context, snapshot) {
                    if (!snapshot.hasData) {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    } else {
                      return buildList(snapshot);
                    }
                  })
            ]));
  }

  Widget buildList(AsyncSnapshot<List<UserActions>> snapshot) {
    return ListView.builder(
        shrinkWrap: true,
        scrollDirection: Axis.vertical,
        itemCount: snapshot.data.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          return UserActionCard(snapshot.data[index]);
        });
  }
}
