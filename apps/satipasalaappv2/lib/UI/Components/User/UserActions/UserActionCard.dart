import 'package:flutter/material.dart';
import 'package:satipasalaappv2/Firebase/model/Action.dart';
import 'package:intl/intl.dart';

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class UserActionCard extends StatefulWidget {
  final UserActions userActions;
  const UserActionCard(this.userActions);

  @override
  _UserActionCardState createState() => _UserActionCardState();
}

class _UserActionCardState extends State<UserActionCard> {
  String image = "";
  String title = "";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.userActions.actionType.contains("session")) {
      image = "assets/01.jpg";
    } else if (widget.userActions.actionType.contains("program")) {
      image = "assets/02.jpg";
    } else if (widget.userActions.actionType.contains("course")) {
      image = "assets/medi04.jpeg";
    }
    title = widget.userActions.actionType
        .replaceAll('_', ' ')
        .toString()
        .trim()
        .capitalize();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CircleAvatar(
                  radius: 20,
                  backgroundImage:
                      NetworkImage(widget.userActions.user.photoURL)),
              SizedBox(
                width: 12,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.userActions.user.displayName,
                      style: TextStyle(fontSize: 16, color: Colors.white)),
                  Text(widget.userActions.user.email,
                      style: TextStyle(fontSize: 12, color: Colors.grey))
                ],
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: 3,
              ),
              Text(title,
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontStyle: FontStyle.italic)),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: Image.asset(
              image,
              height: MediaQuery.of(context).size.height * 0.3,
              width: MediaQuery.of(context).size.width * 0.9,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          // Row(
          //   children: [
          //     Padding(
          //       padding: const EdgeInsets.only(right: 10),
          //       child: Icon(
          //         Icons.filter_vintage,
          //         color: Colors.amber,
          //       ),
          //     ),
          //     Padding(
          //       padding: const EdgeInsets.only(right: 10),
          //       child: Text(
          //         "Level 01",
          //         style: TextStyle(color: Colors.grey),
          //       ),
          //     ),
          //     Text(
          //       "#tags",
          //       style: TextStyle(color: Colors.blueAccent),
          //     )
          //   ],
          // ),
          // Container(
          //   padding: EdgeInsets.all(5),
          //   width: MediaQuery.of(context).size.width * 0.9,
          //   child: DescriptionTextWidget(
          //       text:
          //           "this is a sample text to test out the description widget feature and the show more button feature testing testing testing"),
          // )
          SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }
}
