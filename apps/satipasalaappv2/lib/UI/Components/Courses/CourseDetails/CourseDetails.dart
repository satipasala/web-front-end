import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:satipasalaappv2/Firebase/model/Activity.dart';
import 'package:satipasalaappv2/UI/Components/Home/ActivityFeed/ActivityCard.dart';
import 'package:satipasalaappv2/UI/Components/Sati/ActivityFeed/SatiActivityCard.dart';
import 'package:satipasalaappv2/UI/Styles/AppStyles.dart';
import '../../Home/ActivityFeed/DescriptionWidget.dart';
import '../CourseDetails/CourseActivities/CourseActivityFeed.dart';
import '../../../../Firebase/model/Course.dart';

class CourseDetails extends StatefulWidget {
  final CourseInfo courseInfo;

  CourseDetails({Key key, @required this.courseInfo}) : super(key: key);
  @override
  _CourseDetailsState createState() => _CourseDetailsState();
}

class _CourseDetailsState extends State<CourseDetails>
    with SingleTickerProviderStateMixin {
  // final List<Tab> myTabs = <Tab>[
  //   new Tab(text: 'Pick an activity'),
  //   new Tab(text: 'Comments'),
  // ];
  // TabController _controller;

  var activityMap;
  List<Activity> activityList;
  @override
  void initState() {
    // TODO: implement initState
    activityMap = HashMap.from(widget.courseInfo.activities);
    var temp = new Map<String, dynamic>.from(activityMap);
    activityList =
        temp.entries.map((e) => Activity.fromSnapshot(e.value)).toList();

    super.initState();
    // _controller = new TabController(length: 2, vsync: this);
  }

  // @override
  // void dispose() {
  //   _controller.dispose();
  //   super.dispose();
  // }

  Widget buildActivityCards() {
    final orientation = MediaQuery.of(context).orientation;
    return Expanded(
      child: GridView.builder(
          scrollDirection: Axis.vertical,
          itemCount: activityList.length,
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2, childAspectRatio: 0.8),
          itemBuilder: (BuildContext context, int index) {
            return new GridTile(
              child: SatiActivityCard(activityList[index]),
              //just for testing, will fill with image later
            );
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff03174C),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Stack(
              children: [
                Image.asset('assets/sati/image04.jpg'),
                Positioned(
                  top: 40,
                  left: 20.0,
                  child: InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.arrow_back,
                          color: Colors.black45,
                          size: 30,
                        )),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        "COURSE",
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.amberAccent,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        widget.courseInfo.name,
                        textAlign: TextAlign.start,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.normal,
                            fontSize: 26),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 12,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      DescriptionTextWidget(text: widget.courseInfo.description)
                    ],
                  ),
                  // SizedBox(
                  //   height: 12,
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.start,
                  //   children: [
                  //     Image.asset('assets/headphone.png'),
                  //     SizedBox(
                  //       width: 10,
                  //     ),
                  //     Text(
                  //       "2434 Enrolled",
                  //       textAlign: TextAlign.start,
                  //       style: TextStyle(
                  //           color: Colors.white70,
                  //           fontWeight: FontWeight.normal,
                  //           fontSize: 12),
                  //     ),
                  //   ],
                  // ),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text("Activities", style: subHeadingTextStyle),
                    ],
                  ),

                  Container(
                      width: MediaQuery.of(context).size.width * 0.96,
                      child: buildActivityCards()),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            // new TabBar(
            //   controller: _controller,
            //   tabs: myTabs,
            // ),
            // Expanded(
            //   child: TabBarView(
            //     children: [CourseActivityFeed(), Text('Person')],
            //     controller: _controller,
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}
