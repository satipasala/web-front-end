import 'package:flutter/material.dart';
import '../CourseCard/CourseCard.dart';
import '../../../../Firebase/model/Course.dart';
import '../../../../Firebase/FirebaseDataSource.dart';
import '../../../../Firebase/CollectionService.dart';

class CourseFeed extends StatefulWidget {
  @override
  _CourseFeedState createState() => _CourseFeedState();
}

class _CourseFeedState extends State<CourseFeed> {
  num _pageSize = 10;
  ScrollController _scrollController;
  FirebaseDataSource _firebaseDataSource;
  @override
  void initState() {
    _firebaseDataSource = FirebaseDataSource<CourseInfo>(
        _pageSize,
        CollectionService(
            "courses",
            (Map<String, dynamic> snapshot, [String id]) =>
                CourseInfo.fromSnapshot(snapshot, id)));
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    _firebaseDataSource.nextBatch();
    super.initState();
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the bottom"
      _firebaseDataSource
          .nextBatch(); //todo fix this next batch call since its not efficient.
    }
    if (_scrollController.offset <=
            _scrollController.position.minScrollExtent &&
        !_scrollController.position.outOfRange) {
      // "reach the top";

    }
  }

  Widget buildList(AsyncSnapshot<List<CourseInfo>> snapshot) {
    return GridView.builder(
        scrollDirection: Axis.vertical,
        itemCount: snapshot.data.length,
        controller: _scrollController,
        shrinkWrap: true,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 0.8,
          crossAxisSpacing: 1,
          crossAxisCount: 2,
        ),
        itemBuilder: (BuildContext context, int index) {
          return CourseCard(snapshot.data[index]);
        });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: StreamBuilder<List<CourseInfo>>(
            stream: _firebaseDataSource.connect(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return buildList(snapshot);
              }
            }));
  }
}
