import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:satipasalaappv2/Firebase/AuthService.dart';
import '../Home/BottomNavigation.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Color(0xff03174c),
            padding: const EdgeInsets.only(
              top: 30,
              bottom: 100,
            ),
            child: Stack(
              children: [
                Positioned(
                  bottom: 0.0,
                  right: 0.0,
                  left: 0.0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      new Text(
                        "Forgot Password?",
                        style: TextStyle(color: Colors.white, fontSize: 14),
                      ),
                      // new Text(
                      //   "Signup",
                      //   style: TextStyle(color: Colors.white, fontSize: 16),
                      // )
                    ],
                  ),
                ),
                Positioned(
                    bottom: 120.0,
                    right: 50.0,
                    left: 50.0,
                    child: Container(
                      height: 50,
                      child: RaisedButton(
                        color: Color(0xff8E97FD),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        onPressed: () {
//'sameeraroshanuom@gmail.com', '123456'
                          context
                              .read<AuthService>()
                              .signIn(
                                  emailController.text, passwordController.text)
                              .then((value) {
                            if (value != null) {
                              Navigator.of(context)
                                  .popUntil((route) => route.isFirst);
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          BottomNavigation()));
                            }
                          });
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(color: Color(0xffE6E7F2)),
                        ),
                      ),
                    )),
                Positioned(
                    bottom: 300.0,
                    right: 50.0,
                    left: 50.0,
                    child: Container(
                        child: TextFormField(
                      controller: emailController,
                      decoration: InputDecoration(
                          labelText: 'Enter your email',
                          filled: true,
                          fillColor: Color(0xffF2F3F7),
                          border: new OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(8.0),
                            ),
                          )),
                    ))),
                Positioned(
                    bottom: 220.0,
                    right: 50.0,
                    left: 50.0,
                    child: Container(
                        child: TextFormField(
                      controller: passwordController,
                      decoration: InputDecoration(
                          labelText: 'Enter your password',
                          filled: true,
                          fillColor: Color(0xffF2F3F7),
                          border: new OutlineInputBorder(
                            borderRadius: const BorderRadius.all(
                              const Radius.circular(8.0),
                            ),
                          )),
                    ))),
                Positioned(
                    bottom: 50.0,
                    right: 50.0,
                    left: 50.0,
                    child: Container(
                      height: 50,
                      child: RaisedButton(
                          color: Color(0xff03174c),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Color(0xff8E97FD))),
                          onPressed: () {},
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Image.asset(
                                'assets/google_icon.png',
                                scale: 20,
                              ),
                              Text(
                                "Login with Google",
                                style: TextStyle(color: Color(0xffE6E7F2)),
                              ),
                            ],
                          )),
                    )),
                Positioned(
                    top: 100.0,
                    left: 100.0,
                    child: Container(
                        child: Text("Welcome Back",
                            style: TextStyle(
                                fontSize: 25,
                                fontWeight: FontWeight.w500,
                                color: Color(0xffE6E7F2))))),
                Positioned(
                  top: 30.0,
                  left: 20.0,
                  child: InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Container(
                        decoration: BoxDecoration(
                          color: Color(0xffE6E7F2),
                          shape: BoxShape.circle,
                        ),
                        child: Icon(
                          Icons.arrow_back,
                          color: Color(0xff3F414E),
                          size: 40,
                        )),
                  ),
                )
              ],
            )));
  }
}
