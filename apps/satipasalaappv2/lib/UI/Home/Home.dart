import 'package:flutter/material.dart';
import '../Components/Home/ActivityFeed/ActivityFeed.dart';
import '../Styles/AppStyles.dart';
import '../Components/Events/UpcomingEventFeed.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: darkBackgroundColor,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 50),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                EventFeed(),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Activity Feed",
                      style: headerTextStyle,
                    ),
                    // InkWell(
                    //     onTap: () async {},
                    //     child: Text(
                    //       "See All",
                    //       style: TextStyle(color: Color(0xff7583CA), fontSize: 20),
                    //     ))
                  ],
                ),
                ActivityFeed()
              ],
            ),
          ),
        ));
  }
}
