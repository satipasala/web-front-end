import "dart:async";
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:satipasalaappv2/Firebase/model/base/FirestoreDocument.dart';

import 'Firebase.dart';

typedef Query QueryFn(Query ref);
typedef FirestoreDocument FromSnapshot(Map<String, dynamic> snapshot,[String id]); /// https://github.com/dart-lang/language/issues/356 T.fromSnapshot cannot be called yet.

class CollectionService<T extends FirestoreDocument> {
  bool reachedEnd = false;
  QueryDocumentSnapshot lastDoc;
  String collection;
  FirebaseFirestore fireStore = FirebaseFirestore.instance;
  FirebaseStorage storage = FirebaseStorage.instance;
  FromSnapshot fromSnapshot;
  CollectionService(this.collection,this.fromSnapshot);

  setCollection(String collectionName) {
    this.collection = collectionName;
  }

  Future<DocumentReference> add(doc) {
    return this.fireStore.collection(this.collection).add(doc);
  }

  //todo generify component and remove any
  Future addWithId(doc) {
    return this.fireStore.collection(this.collection).doc(doc.id).set(doc);
  }

  Future update(String id, doc) {
    return this.fireStore.collection(this.collection).doc(id).update(doc);
  }

  Future delete(id) {
    return this.fireStore.collection(this.collection).doc(id).delete();
  }

  /**
   * get the collection reference of given collection name.
   */
  Stream<T> get(String documentId, [QueryFn queryFn]) {
    /*return getCollectionRef(queryFn)
        .doc(documentId)
        .snapshots()
        .handleError((e) => FlutterError.dumpErrorToConsole(e))
        .map((event) => this.getDocumentData(event));*/

    return this
        .fireStore
        .collection(this.collection)
        .doc(documentId)
        .snapshots()
        .handleError((e) => FlutterError.dumpErrorToConsole(e))
        .map((event) => this.getDocumentData(event));
  }

  T getDocumentData( DocumentSnapshot  payload) {

    return fromSnapshot(payload.data(),payload.id);
  }

  Query getCollectionRef(QueryFn queryFn) {
    Query collectionRef;
    if (queryFn != null) {
      collectionRef = queryFn(this.fireStore.collection(this.collection));
    } else {
      collectionRef = this.fireStore.collection(this.collection);
    }

    return collectionRef;
  }

  /**
   * get a sumb collection of main collection
   */
  Stream<List<T>> queryCollection(QueryFn queryFn) {
    return this.onSnapshotChanges(getCollectionRef(queryFn));
  }

  Stream<List<T>> onSnapshotChanges(Query collection) {
    return collection
        .snapshots()
        //.handleError((e) => FlutterError.dumpErrorToConsole(e))
        .map((QuerySnapshot<Object> snapshot) {
      return snapshot.docs.map<T>((QueryDocumentSnapshot document) {
        return  fromSnapshot(document.data(),document.id);
      }).toList();
    });
  }

  Stream<List<T>> querySubCollection(QueryFn queryFn,
      [List<SubCollectionInfo> subCollectionPaths]) {
    Query collection;
    if (subCollectionPaths != null && subCollectionPaths.length > 0) {
      for (var i = 0; i < subCollectionPaths.length; i++) {
        if (collection != null) {
          collection =
              this.getSubCollection(collection, subCollectionPaths[i], queryFn);
        } else {
          collection = this.getSubCollection(
              this.fireStore.collection(this.collection),
              subCollectionPaths[i],
              queryFn);
        }
      }
    } else {
      collection = getCollectionRef(queryFn);
    }
    return this.onSnapshotChanges(collection);
  }

  /**
   * get a sub collection from a given collection
   *
   *
   */
  CollectionReference getSubCollection(
      CollectionReference collection, SubCollectionInfo subCollection,
      [QueryFn queryFn]) {
    if (queryFn != null) {
      return queryFn(collection
          .doc(subCollection.documentId)
          .collection(subCollection.subCollection));
    } else {
      return collection
          .doc(subCollection.documentId)
          .collection(subCollection.subCollection);
    }
  }

/*  */ /**
        * get data from document change action.
        *
        */ /*
  T getPayload(DocumentChangeAction<DocumentData> action) {
    if (action.payload != null) {
      final T data =;
      data [ "id" ] =;
      //  this.lastObj = data

      //return this.lastObj;
      this.lastDoc = action.payload.doc;
      return data;
    } else {
      final T u1 = null;
      return u1;
    }
  }*/

/*
  */ /**
      * Return all documents in the collection
      *
      */ /*
  Observable<List <T>> getAll() {
    console.warn(
        "**Important** Getting all documents are very costly.Should be removed ASAP.collection:",
        this.collectionName);
    return this.fireStore.collection */ /*< T >*/ /*(this.collection).valueChanges();
  }

  Future setDoc(id, T doc) {
    return this.fireStore.collection(this.collection).doc(id).set(doc);
  }*/
}
