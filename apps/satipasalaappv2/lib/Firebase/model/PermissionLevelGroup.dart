import "PermissionLevel.dart" show PermissionLevel;
import './base/FirestoreDocument.dart';

 class PermissionLevelGroup extends FirestoreDocument {
  PermissionLevel view;
  PermissionLevel edit;

  PermissionLevelGroup.fromSnapshot(Map<String, dynamic> snapshot) : super.fromSnapshot(snapshot);

  dynamic getProp(String key) => <String, dynamic>{
    'view' : this.view,
    'edit':this.edit
  }[key];
}
