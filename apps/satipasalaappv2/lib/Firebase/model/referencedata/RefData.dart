import 'package:satipasalaappv2/Firebase/model/base/FirestoreDocument.dart';

abstract class RefData extends FirestoreDocument {
  String name;
  String active;

  RefData.fromSnapshot(Map<String, dynamic> snapshot)
      : super.fromSnapshot(snapshot);
}
