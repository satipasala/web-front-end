import 'package:satipasalaappv2/Firebase/model/Course.dart';

import "FeedbackForm.dart" show FeedbackForm;
import "RefData.dart" show RefData;
// import "../Activity.dart" show Activity;

class Program extends RefData {
  String id;
  String name;
  String description;
  // List<Activity> activities;
  Map<dynamic, dynamic> courses;
  // num facilitatorsCount;
  // num feedbackCollectionCount;
  // List<FeedbackForm> feedbackForms;
  Program.fromSnapshot(Map<String, dynamic> snapshot)
      : id = snapshot['id'],
        super.fromSnapshot(snapshot);
}
