import "referencedata/ActivityType.dart" show ActivityType;
import 'package:satipasalaappv2/Firebase/model/referencedata/RefData.dart';
import 'base/FirestoreDocument.dart';
import 'User.dart';

class UserActions extends FirestoreDocument {
  String id;
  String actionType;
  String date;
  User user;
  dynamic record;

  UserActions.fromSnapshot(Map<String, dynamic> snapshot, [String id])
      : this.id = id,
        actionType = snapshot['actionType'],
        date = snapshot['date'],
        user = User.fromSnapshot(snapshot['user']),
        record = snapshot['record'],
        super.fromSnapshot(snapshot, id);
}
