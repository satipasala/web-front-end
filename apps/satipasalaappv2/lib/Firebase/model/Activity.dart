import "referencedata/ActivityType.dart" show ActivityType;
import 'package:satipasalaappv2/Firebase/model/referencedata/RefData.dart';
import 'base/FirestoreDocument.dart';

class Activity extends FirestoreDocument {
  String id;
  String name;
  String active;
  String description;
  ActivityType type;
  num maxPoints;
  dynamic contentType;
  String gradable;
  dynamic resource;

  Activity.fromSnapshot(Map<String, dynamic> snapshot, [String id])
      : this.id = id,
        name = snapshot['name'],
        active = snapshot['active'],
        description = snapshot['coordinatorInfo'],
        resource = snapshot['resource'],
        type = ActivityType.fromSnapshot(snapshot['type']),
        super.fromSnapshot(snapshot, id);
}
