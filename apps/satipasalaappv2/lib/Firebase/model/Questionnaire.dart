import "Question.dart" show Question;
import 'base/FirestoreDocument.dart';

class Questionnaire extends FirestoreDocument {
  /*type:QuestionnaireType;*/
  String name;
  String id;
  FirestoreDocument questions;
  List<String> questionsIdArray;
  num occurence;

  Questionnaire.fromSnapshot(Map<String, dynamic> snapshot) : super.fromSnapshot(snapshot);
}

enum QuestionnaireType { AGGRIGATED, INDIVIDUAL }
