import 'package:satipasalaappv2/Firebase/model/base/FirestoreDocument.dart';

abstract class ParticipationInfo extends FirestoreDocument {
  dynamic /* num |  */ numberOfParticipants = 0;
  dynamic /* num |  */ numberOfAdults = 0;
  dynamic /* num |  */ numberOfChildren = 0;
  dynamic /* num |  */ numberOfMales = 0;
  dynamic /* num |  */ numberOfFemales = 0;

  ParticipationInfo.fromSnapshot(Map<String, dynamic> snapshot) : super.fromSnapshot(snapshot);
}
