import 'dart:collection';

import 'package:satipasalaappv2/Firebase/model/base/FirestoreDocument.dart';
import 'base/FirestoreDocument.dart';

class EventSession extends FirestoreDocument {
  String id;
  String name;
  String eventId;
  var startDate;
  String startTime;
  dynamic status;
  dynamic program;
  var imgUrls;
  EventSession.fromSnapshot(Map<String, dynamic> snapshot, [String id])
      : this.id = id,
        eventId = snapshot['eventId'],
        name = snapshot['name'].trim(),
        startDate = snapshot['startDate'],
        imgUrls = snapshot['imgUrls'],
        status = snapshot['status'],
        program = snapshot['program'],
        super.fromSnapshot(snapshot, id);
}
