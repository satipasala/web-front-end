import "Location.dart" show LocationInfo;
import "AddressInfo.dart" show AddressInfo;
import "Host.dart" show HostInfo;
import "Role.dart" show Role;
import "./base/FirestoreDocument.dart";

class User extends UserInfo{
  HostInfo organizationInfo;
  dynamic /* String | null */ preferredMedium;
  bool disabled;
  dynamic /* String | null */ createdAt;
  dynamic /* String | null */ updatedAt;
  Object courseSubscriptions;
  String description;
  Role userRole;

  User.fromSnapshot(Map<String, dynamic> snapshot,[String id]) :
        organizationInfo=HostInfo.fromSnapshot(snapshot['organizationInfo']),
        preferredMedium=snapshot['preferredMedium'],
        courseSubscriptions = snapshot['courseSubscriptions'],
        userRole = Role.fromSnapshot(snapshot['userRole']),
        super.fromSnapshot(snapshot,id);
}

//abstract class UserInfo implements FirebaseAuth.User {
class UserInfo extends FirestoreDocument {

  String email;
  String displayName;
  String emailVerified;
  String isAnonymous;
  String metadata;
  String phoneNumber;
  String photoURL;
  String providerData;
  String refreshToken;
  String tenantId;
  String uid;
  dynamic /* String | null */ userName;
  dynamic /* String | null */ firstName;
  dynamic /* String | null */ lastName;
  dynamic /* String | null */ dob;
  dynamic /* String | null */ nic;
  String userRoleId;
  AddressInfo addressInfo;
  HostInfo organizationInfo;
  LocationInfo locationInfo;

  UserInfo.fromSnapshot(Map<String, dynamic> snapshot,[String id]) :
        email=snapshot['email'],
        displayName=snapshot['displayName'],
        photoURL=snapshot['photoURL'],
        uid=snapshot['uid'],
        super.fromSnapshot(snapshot,id);
}
