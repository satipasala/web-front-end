import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'UsersService.dart';
import './model/User.dart' as firestore_user;

abstract class BaseAuth {
  Future<String> signIn(String email, String password);

  Future<String> signUp(String email, String password);

  Future<User> getCurrentUser();

  Future<void> sendEmailVerification();

  Future<void> signOut();

  Future<bool> isEmailVerified();
}

class AuthService implements BaseAuth {
  final FirebaseAuth _firebaseAuth;
  UsersService _usersService;

  AuthService(this._firebaseAuth) {
    this._usersService = UsersService();
  }

  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<String> signIn(String email, String password) async {
    try {
      UserCredential result = await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      User user = result.user;
      return user.uid;
    } catch (ex) {
      return ex;
    }
  }

  Future<String> signUp(String email, String password) async {
    UserCredential result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
    User user = result.user;
    return user.uid;
  }

  Future<User> getCurrentUser() async {
    User user = _firebaseAuth.currentUser;
    return user;
  }

  Future<void> signOut() async {
    return _firebaseAuth.signOut();
  }

  Future<void> sendEmailVerification() async {
    User user = _firebaseAuth.currentUser;
    user.sendEmailVerification();
  }

  Future<bool> isEmailVerified() async {
    User user = _firebaseAuth.currentUser;
    return user.emailVerified;
  }

  Future<firestore_user.User> getCurrentDbUser() async {
    Completer<firestore_user.User> completer =
        new Completer<firestore_user.User>();
    Stream<User> stream = _firebaseAuth.authStateChanges();
    stream.forEach((user) {
      if (user == null) {
        completer.complete(null);
      } else {
        var newUser = this._usersService.get(user.email);
        newUser.forEach((temp) {
          if (user != null) {
            completer.complete(temp);
          }
        });
      }
    });
    // for (User loggedInUser in stream) {
    //   if (loggedInUser == null) {
    //     completer.complete(null);
    //   } else {
    //     await for (firestore_user.User user
    //         in this._usersService.get(loggedInUser.email)) {
    //       if (user != null) {
    //         completer.complete(user);
    //         break;
    //       }
    //     }
    //   }
    // }
    return completer.future;
  }
}
