import 'dart:async';
import 'dart:developer' as developer;

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:rxdart/rxdart.dart';

import 'CollectionService.dart';
import 'Firebase.dart';
import 'SDataSource.dart';
import 'model/base/FirestoreDocument.dart';
/**
 * Data source for the HostInfoComponent view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */

typedef Query DataQuery(CollectionReference value);

class DataRequest {
  DataQuery query;
  List<SubCollectionInfo> subCollectionPaths;

  DataRequest(this.query, [this.subCollectionPaths]);
}

class PageEvent {
  /** The current page index. */
  int pageIndex;

  /**
   * Index of the page that was selected previously.
   * @breaking-change 8.0.0 To be made into a required property.
   */
  int previousPageIndex;

  /** The current page size */
  int pageSize;

  /** The current total number of items being paged */
  int length;

  PageEvent(this.pageSize,
      [this.pageIndex, this.length, this.previousPageIndex]);
}

class FirebaseDataSource<T extends FirestoreDocument> extends SDataSource<T> {
  int pageSize;
  var total = 100;
  T lastObj;
  StreamController<List<T>> dataStream = StreamController<List<T>>();
  CollectionService<T> collectionService;

  //collection filters
  List<FilterGroup> filterGroups = [];
  List<SearchFilter> searchFilters = [];
  StreamController<bool> filterChangeStream = StreamController<bool>();
  List<OrderBy> orderBy = [];
  num batchSize = 20;
  StreamController<DataRequest> dataRquestStream =
      StreamController<DataRequest>();

  FirebaseDataSource(this.pageSize, this.collectionService) : super() {
    /* super call moved to initializer */
    // wait for 1000 ms to send requests to server. avaoids unwanted server load
    this.dataRquestStream.stream.listen((value) {
      developer.log("request recieved value");
      this._requestData(value.query, value.subCollectionPaths);
    });
    this.filterChangeStream.stream.listen((value) {
      this.fetchData();
    });
  }

  bool reachedEnd() {
    return this.collectionService.reachedEnd;
  }

  /**
   * query data of a sub collection by giving documentId and sub collection name. this should never
   * be called directly in order to reduce number of server calls.
   *
   *
   */
  _requestData(DataQuery query, [List<SubCollectionInfo> subCollectionPaths]) {
    final List<Stream<List<T>>> observableList = [];
    if (this.searchFilters.length > 0) {
      this.searchFilters.forEach((searchFilter) {
        if (this.filterGroups.length > 0) {
          this.queryBySearchFiltersAndFilterGroup(
            observableList,
            query,
            searchFilter,
          );
        } else {
          this.queryBySearchFilters(
            observableList,
            query,
            searchFilter,
          );
        }
      });
    } else if (this.filterGroups.length > 0) {
      this.queryByFilterGroup(observableList, query, subCollectionPaths);
    } else {
      final QueryFn queryFnc = (ref) => query(this.addOrderByClauses(ref));
      observableList.add(this
          .collectionService
          .querySubCollection(queryFnc, subCollectionPaths));
    }

    CombineLatestStream.list(observableList).listen((documentList) =>
        documentList.forEach((documents) => this.dataStream.add(documents)));
  }

  /**
   * query data method sends the query to data Stream .
   * equests are throttled and last request will be sent to the server
   *
   *
   */
  queryData(DataQuery query, [List<SubCollectionInfo> subCollectionPaths]) {
    this.dataRquestStream.add(DataRequest(query, subCollectionPaths));
  }

  queryBySearchFiltersAndFilterGroup(
      List<Stream<dynamic>> observableList, DataQuery query, searchFilter,
      [List<SubCollectionInfo> subCollectionPaths]) {
    this.filterGroups.forEach((filterGroup) {
      final QueryFn queryFnc = (ref) => query(this.addSearchWhereClause(
          this.addWhereClauses(filterGroup.filters,
              this.addSearchAndOrderByClauses(ref, searchFilter)),
          searchFilter));
      observableList.add(this.collectionService.querySubCollection(
            queryFnc,
          ));
    });
  }

  queryBySearchFilters(
      List<Stream<dynamic>> observableList, DataQuery query, searchFilter,
      [List<SubCollectionInfo> subCollectionPaths]) {
    final QueryFn queryFnc = (ref) => query(this.addSearchWhereClause(
        this.addSearchAndOrderByClauses(ref, searchFilter), searchFilter));
    observableList.add(this.collectionService.querySubCollection(
          queryFnc,
        ));
  }

  queryByFilterGroup(List<Stream<dynamic>> observableList, DataQuery query,
      [List<SubCollectionInfo> subCollectionPaths]) {
    this.filterGroups.forEach((filterGroup) {
      final QueryFn queryFnc = (ref) => query(this
          .addWhereClauses(filterGroup.filters, this.addOrderByClauses(ref)));
      observableList.add(this
          .collectionService
          .querySubCollection(queryFnc, subCollectionPaths));
    });
  }

  /**
   * query data from multiple sub collections by giving documentId and sub collection name as rest parameters
   */
  queryCombinedData(
      QueryFn queryFn, List<List<SubCollectionInfo>> subCollectionPaths) {
    for (var i = 0; i < subCollectionPaths.length; i++) {
      this.queryData(queryFn, subCollectionPaths[i]);
    }
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   *
   */
  Stream<List<T>> connect([CollectionViewer collectionViewer]) {
    this.collectionService.lastDoc = null;
    return this.dataStream.stream;
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect([CollectionViewer collectionViewer]) {
    dataStream.close();
    dataRquestStream.close();
    filterChangeStream.close();
  }

  /**
   * load more data to collection using paginator
   *
   */
  loadMore(PageEvent event) {
    this.queryData((query) => query
        .startAfterDocument(this.collectionService.lastDoc)
        .limit(event.pageSize));
  }

  void fetchData() {
    //todo add correct value to startAt() method
    if (this.pageSize != null) {
      this.queryData((query) => query.limit(this.pageSize));
    } else {
      this.queryData((query) => query.limit(this.batchSize));
    }
  }

  nextBatch() {
    if (this.collectionService.lastDoc != null) {
      this.queryData((query) => query
          .startAfterDocument(this.collectionService.lastDoc)
          .limit(this.batchSize));
    } else {
      this.queryData((query) => query.limit(this.batchSize));
    }
  }

  setOrderBy(List<OrderBy> orderBy) {
    this.orderBy = orderBy;
  }

  addOrderBy(OrderBy orderBy) {
    this.orderBy.add(orderBy);
  }

  OrderBy getDefaultOrderBy(SearchFilter searchFilter) {
    final index = this
        .orderBy
        .indexWhere((orderBy) => orderBy.fieldPath == searchFilter.field);
    final newOrderBy = ({
      "fieldPath": searchFilter.field,
      "directionStr": OrderByDirection.asc
    } as OrderBy);
    if (index > -1) {
      final existingObject = this.orderBy.removeAt(index);
      newOrderBy.directionStr = existingObject.directionStr;
    }
    return newOrderBy;
  }

  Query addSearchAndOrderByClauses(Query query, SearchFilter searchFilter) {
    final order = this.getDefaultOrderBy(searchFilter);
    return this.addOrderByClause(query, order);
  }

  Query addOrderByClauses(Query query) {
    this.orderBy.forEach((order) {
      query = this.addOrderByClause(query, order);
    });
    return query;
  }

  Query addOrderByClause(Query query, OrderBy orderBy) {
    return query.orderBy(orderBy.fieldPath,
        descending: orderBy.directionStr == OrderByDirection.desc);
  }

  Query addWhereClauses(List<Filter> filters, Query query) {
    filters.forEach((filter) {
      query = this.addWhereClause(query, filter);
    });
    return query;
  }

  Query addWhereClause(Query query, Filter filter) {
    return query.where(filter);
  }

  Query addSearchWhereClause(Query query, SearchFilter filter) {
    //return query.where(filter.field, ">=", filter.value)
    return query
        .where(filter.field, isGreaterThanOrEqualTo: filter.value)
        .where(filter.field, isLessThan: filter.value + "z");
  }

  setFilterGroups(List<FilterGroup> filterGroups) {
    this.filterGroups = [];
    this.filterGroups = filterGroups;
    this.filterChangeStream.add(true);
  }

  accumulateFilterGroup(FilterGroup filterGroup) {
    filterGroup.filters.forEach((filter) {
      this.removeFilter(filterGroup.key, filter, false);
      this.filterGroups.add(filterGroup);
    });
    this.filterChangeStream.add(true);
  }

  accumulateFilter(String groupName, Filter filter) {
    this.removeFilter(groupName, filter, false);
    this.filterGroups.forEach((value) {
      if (value.key == groupName) {
        value.filters.add(filter);
      }
    });
    this.filterChangeStream.add(true);
  }

  removeFilterGroup(FilterGroup filterGroup, [bool fireChanges = true]) {
    final index =
        this.filterGroups.indexWhere((value) => (value.key == filterGroup.key));
    if (index != -1) {
      this.filterGroups.remove(index);
    }
    if (fireChanges == true) {
      this.filterChangeStream.add(false);
    }
  }

  removeFilter(String groupName, Filter filter, [bool fireChanges = true]) {
    this.filterGroups.forEach((group) {
      if (group.key == groupName) {
        final index = group.filters.indexWhere((value) => value == filter);
        if (index != -1) {
          group.filters.remove(index);
        }
      }
    });
    if (fireChanges == true) {
      this.filterChangeStream.add(false);
    }
  }

  clearFilterGroup(FilterGroup filterGroup) {
    filterGroup.filters.forEach((filter) {
      this.removeFilter(filterGroup.key, filter, true);
    });
    this.filterChangeStream.add(true);
  }

  setSearchFilters(List<SearchFilter> filters) {
    this.searchFilters = [];
    filters.forEach((filter) {
      this.searchFilters.add(filter);
    });
    this.filterChangeStream.add(true);
  }

  accumulateSearchFilters(List<SearchFilter> filters) {
    filters.forEach((filter) {
      this.removeSearchFilter(filter, false);
      this.searchFilters.add(filter);
    });
    this.filterChangeStream.add(true);
  }

  accumulateSearchFilter(SearchFilter filter) {
    this.removeSearchFilter(filter, false);
    this.searchFilters.add(filter);
    this.filterChangeStream.add(true);
  }

  removeSearchFilter(SearchFilter filter, [bool fireChanges = true]) {
    final index =
        this.searchFilters.indexWhere((value) => (value.field == filter.field));
    if (index != -1) {
      this.searchFilters.remove(index);
    }
    if (fireChanges == true) {
      this.filterChangeStream.add(false);
    }
  }

  clearSearchFilters(List<SearchFilter> filters) {
    filters.forEach((filter) {
      this.removeSearchFilter(filter, true);
    });
    this.filterChangeStream.add(true);
  }

  clearAllFilters() {
    final List<FilterGroup> filterGroups = List.from(this.filterGroups);
    filterGroups.forEach((group) {
      this.removeFilterGroup(group, false);
    });
    final searchFilters = List.from(this.searchFilters);
    searchFilters.forEach((filter) {
      this.removeSearchFilter(filter, false);
    });
    this.filterChangeStream.add(true);
  }

  setBatchSize(num batchSize) {
    this.batchSize = batchSize;
  }
}
