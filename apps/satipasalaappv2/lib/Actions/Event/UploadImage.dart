import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';

FirebaseStorage storage = FirebaseStorage.instance;
Future<void> uploadImage(
    String folder, String eventId, String name, File image) async {
  var downloadUrl;

  var snapshot = await storage
      .ref()
      .child('$folder/$eventId/$name')
      .putFile(image)
      .whenComplete(() => print("image uploaded"))
      .then((snapshot) async {
    downloadUrl = await snapshot.ref.getDownloadURL();
  }).onError((error, stackTrace) {
    print(error);
    downloadUrl = "error";
  });

  return downloadUrl;
}
