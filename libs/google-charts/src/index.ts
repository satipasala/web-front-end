export * from './lib/google-charts.module';
export {SPieChart} from "./lib/s-pie-chart/s-pie-chart.component";
export {SGeoChart} from "./lib/s-geo-chart/s-geo-chart.component";
export {ChartInjector} from "./lib/chart-injector";
