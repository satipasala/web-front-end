import { async, TestBed } from '@angular/core/testing';
import { GoogleChartsModule } from './google-charts.module';

describe('GoogleChartsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GoogleChartsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(GoogleChartsModule).toBeDefined();
  });
});
