import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SPieChart } from './s-pie-chart.component';

describe('SPieChartComponent', () => {
  let component: SPieChart;
  let fixture: ComponentFixture<SPieChart>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SPieChart ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SPieChart);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
