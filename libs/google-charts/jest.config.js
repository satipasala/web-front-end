module.exports = {
  name: 'google-charts',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/google-charts',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
