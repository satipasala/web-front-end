export interface ParticipationInfo {
    numberOfParticipants: number | 0;
    numberOfAdults: number | 0;
    numberOfChildren: number | 0;
    numberOfMales: number | 0;
    numberOfFemales: number | 0;
}