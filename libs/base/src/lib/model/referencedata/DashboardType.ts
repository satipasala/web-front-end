export class DashboardType {
  id: string;
  name : string;
  active : string;
  dashboards?:{name:string,link:string}[]
}
