import {RefData} from "./RefData";

export interface LocationType extends RefData {
  // description?: string;
  // id:string;
  displayName:string;
  // type:string;  //1 (year 1)
  // organizationType:OrganizationType;
}
