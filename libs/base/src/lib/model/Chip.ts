import {ThemePalette} from "@angular/material/core";


export interface Chip {
  name: string;
  color: ThemePalette;
}
