import {PermissionLevel} from "./PermissionLevel";

export interface PermissionLevelGroup{
  view:PermissionLevel;
  edit:PermissionLevel;
}
