export class NavigationItem {
  subIcon: string;
  subCategoryName: string;
  subCategoryLink: string;
  subCategoryQuery?: any;
  visible: boolean;
}
