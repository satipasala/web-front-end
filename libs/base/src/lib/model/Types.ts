export enum AnswerType{
  RARELY_SOMETIMES="RARELY_SOMETIMES",
  ALMOST_NEVER="ALMOST_NEVER",
  NEVER_ALOT="NEVER_ALOT",
  WRITTEN="WRITTEN"
}


export enum FormFieldType{
  text="text",
  radio="radio",
  dropdown="dropdown",
  checkbox="checkbox",
  selectionList="selection-list"
}
