
export interface EventsInfo {

  numberOfAdults:number;
  numberOfChildren:number;
  numberOfFemales:number;
  numberOfMales:number;
  numberOfParticipants:number;
  numberOfEvents:number;
}
