import { ActivityType } from './referencedata/ActivityType';

export class Activity {
  id: string;
  name: string;
  active: string;
  description: string;
  type: ActivityType;
  maxPoints: number;
  gradable: string;
  resource:Object
}
