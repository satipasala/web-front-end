import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SDragDropList } from './s-drag-drop-list.component';

describe('DragDropListComponent', () => {
  let component: SDragDropList;
  let fixture: ComponentFixture<SDragDropList>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SDragDropList ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SDragDropList);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
