import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SInfiniteScrollGrid } from './infinite-scroll-grid.component';

describe('InfiniteScrollComponent', () => {
  let component: SInfiniteScrollGrid;
  let fixture: ComponentFixture<SInfiniteScrollGrid>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SInfiniteScrollGrid ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SInfiniteScrollGrid);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
