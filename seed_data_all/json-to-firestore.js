const admin = require('../node_modules/firebase-admin/lib/index');
const serviceAccount = require("./keys/satipasala-uat-firebase-key");
const databaseURL = "https://satipasala-uat.firebaseio.com";
console.log("Database URL: "+ databaseURL);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: databaseURL,
});

/**
 * Data is a collection if
 *  - it has a odd depth
 *  - contains only objects or contains no objects.
 */
function isCollection(data, path, depth) {
  if (
    typeof data != 'object' || typeof data != 'array' ||
    data == null ||
    data.length === 0 ||
    isEmpty(data)
  ) {
    return false;
  }

  for (const key in data) {
    if (typeof data[key] != 'object' || data[key] == null) {
      // If there is at least one non-object item in the data then it cannot be collection.
      return false;
    }
  }

  return true;
}

// Checks if object is empty.
function isEmpty(obj) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

async function upload(data, path) {
  return await admin.firestore()
    .doc(path.join('/'))
    .set(data)
    .then(() => console.log(`Document ${path.join('/')} uploaded.`))
    .catch((error) => {
      console.error(`Could not write document ${path.join('/')}.`, error);
    });
}

/**
 *
 */
async function resolve(data, path = []) {
  if (path.length > 0 && path.length % 2 == 0) {
    // Document's length of path is always even, however, one of keys can actually be a collection.

    // Copy an object.
    const documentData = Object.assign({}, data);

    for (const key in data) {
      // Resolve each collection and remove it from document data.
      if (isCollection(data[key], [...path, key])) {
        // Remove a collection from the document data.
        delete documentData[key];
        // Resolve a colleciton.
        resolve(data[key], [...path, key]);
      }
    }

    // If document is empty then it means it only consisted of collections.
    if (!isEmpty(documentData)) {
      // Upload a document free of collections.
      await upload(documentData, path);
    }
  } else {
    // Collection's length of is always odd.
    for (const key in data) {
      // Resolve each collection.
      await resolve(data[key], [...path, key]);
    }
  }
}

async function removeDocument(collection, document ){
  admin.firestore()
    .collection(collection)
    .doc(document)
    .delete()
    .then(() => console.log(`Document ${document} is deleted.`))
    .catch((error) => {
      console.error(`Could not delete document ${document}.`, error);
    });
}

//resolve(data);
//resolve(require("./collections/Activities.json"));
//resolve(require("./collections/Courses.json"));
//resolve(require("./collections/Enrollments.json"));
//resolve(require("./collections/Hosts.json"));
 resolve(require("./collections/ReferanceData.json"));
resolve(require("./collections/Roles.json"));
//resolve(require("./collections/UserActivities.json"));
resolve(require("./collections/Users.json"));
resolve(require("./collections/Auth.json"));
//resolve(require("./collections/Questionnaires.json"));
//resolve(require("./collections/Questions.json"));
//resolve(require("./collections/Questionnaires.json"));
//resolve(require("./collections/Questions.json"));
//resolve(require("./collections/country/Cities.json"));
//resolve(require("./collections/country/States.json"));

//removeDocument("referencedata", "cities");
//removeDocument("referencedata", "states");
//removeDocument("referencedata", "countries");
//removeDocument("referencedata", "languages");
//resolve(require("./collections/Cities.json"));
//resolve(require("./collections/States.json"));
//resolve(require("./collections/Countries.json"));
//resolve(require("./collections/Languages.json"));
