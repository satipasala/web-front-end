import * as functions from 'firebase-functions';
import {updateProgramSubscriptions} from "../../utils/actionFeedUtils";

export const dbEventOnUpdate = functions.firestore.document('events/{eventId}').onUpdate(async (change, context) => {
  return new Promise<any>((resolve, reject) => {
    updateProgramSubscriptions( change.after.data(),false).then(resolve).catch(reject);
  });
});




