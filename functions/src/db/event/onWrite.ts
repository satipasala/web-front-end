import * as functions from 'firebase-functions';
import {updateProgramSubscriptions} from "../../utils/actionFeedUtils";

export const dbEventOnWrite = functions.firestore.document('events/{eventId}').onCreate((change, context) => {
  return new Promise((resolve, reject) => {
    const {eventId} = context.params;
    const newEvent = change.data();

    updateProgramSubscriptions(newEvent,true).then(resolve).catch(reject);
  })
});

