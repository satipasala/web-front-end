import * as functions from 'firebase-functions';
import {updateCourseSubscriptions, updateProgramSubscriptions} from "../../utils/actionFeedUtils";

export const dbEventSessionsOnUpdate = functions.firestore.document('eventSessions/{sessionId}')
  .onUpdate(async (change, context) => {
  return new Promise<any>((resolve, reject) => {
    updateProgramSubscriptions( change.after.data(),false).then(resolve).catch(reject);
    updateCourseSubscriptions(change.after.data(),true).then(resolve).catch(reject);
  });
});




