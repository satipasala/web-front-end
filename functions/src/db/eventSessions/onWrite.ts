import * as functions from 'firebase-functions';
import {updateCourseSubscriptions, updateProgramSubscriptions} from "../../utils/actionFeedUtils";

export const dbEventSessionsOnWrite = functions.firestore.document('eventSessions/{sessionId}')
  .onWrite((change, context) => {
  return new Promise((resolve, reject) => {
    updateProgramSubscriptions( change.after.data(),false).then(resolve).catch(reject);
    updateCourseSubscriptions(change.after.data(),true).then(resolve).catch(reject);
  })
});


