const admin = require('../node_modules/firebase-admin/lib/index');
const serviceAccount = require("./keys/satipasala-develop-key");
const databaseURL = "https://satipasala-develop-cbc9a.firebaseio.com";
console.log("Database URL: "+ databaseURL);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: databaseURL,
});

/**
 * Data is a collection if
 *  - it has a odd depth
 *  - contains only objects or contains no objects.
 */
function isCollection(data, path, depth) {
  if (
    typeof data != 'object' || typeof data != 'array' ||
    data == null ||
    data.length === 0 ||
    isEmpty(data)
  ) {
    return false;
  }

  for (const key in data) {
    if (typeof data[key] != 'object' || data[key] == null) {
      // If there is at least one non-object item in the data then it cannot be collection.
      return false;
    }
  }

  return true;
}

// Checks if object is empty.
function isEmpty(obj) {
  for (const key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

async function upload(data, path) {
  return await admin.firestore()
    .doc(path.join('/'))
    .set(data)
    .then(() => console.log(`Document ${path.join('/')} uploaded.`))
    .catch((error) => {
      console.error(`Could not write document ${path.join('/')}.`, error);
    });
}

/**
 *
 */
async function resolve(data, path = []) {
  if (path.length > 0 && path.length % 2 == 0) {
    // Document's length of path is always even, however, one of keys can actually be a collection.

    // Copy an object.
    const documentData = Object.assign({}, data);

    for (const key in data) {
      // Resolve each collection and remove it from document data.
      if (isCollection(data[key], [...path, key])) {
        // Remove a collection from the document data.
        delete documentData[key];
        // Resolve a colleciton.
        resolve(data[key], [...path, key]);
      }
    }

    // If document is empty then it means it only consisted of collections.
    if (!isEmpty(documentData)) {
      // Upload a document free of collections.
      await upload(documentData, path);
    }
  } else {
    // Collection's length of is always odd.
    for (const key in data) {
      // Resolve each collection.
      await resolve(data[key], [...path, key]);
    }
  }
}

async function removeDocument(collection, document ){
  admin.firestore()
    .collection(collection)
    .doc(document)
    .delete()
    .then(() => console.log(`Document ${document} is deleted.`))
    .catch((error) => {
      console.error(`Could not delete document ${document}.`, error);
    });
}

//resolve(data);

removeDocument("referencedata", "cities");
removeDocument("referencedata", "states");
removeDocument("referencedata", "countries");
removeDocument("referencedata", "languages");


resolve(require("./collections/Users.json"));
resolve(require("./collections/Auth.json"));
resolve(require("./collections/Cities.json"));
resolve(require("./collections/Courses.json"));
resolve(require("./collections/Enrollments.json"));
resolve(require("./collections/Events.json"));
resolve(require("./collections/Feedback.json"));
resolve(require("./collections/Files.json"));
resolve(require("./collections/Glossary.json"));
resolve(require("./collections/Hosts.json"));
resolve(require("./collections/Permissions.json"));
resolve(require("./collections/Photos.json"));
resolve(require("./collections/Pictures.json"));
resolve(require("./collections/Questionnaires.json"));
resolve(require("./collections/RefData.json"));
resolve(require("./collections/Roles.json"));
resolve(require("./collections/States.json"));
resolve(require("./collections/User_activities.json"));


//minimal
/*
resolve(require("./collections_minimal/RefData.json"));
resolve(require("./collections_minimal/Roles.json"));
resolve(require("./collections_minimal/Users.json"));*/
