const firestoreService = require('firestore-export-import');
const serviceAccount = require('./keys/satipasala-org-service-key');
const databaseURL = "https://satipasala-org.firebaseio.com";
const fs = require('fs');

// Initiate Firebase App
firestoreService.initializeApp(serviceAccount, databaseURL);

console.log("App Initialized");
// Start exporting your data
/*
firestoreService
  .backup('collection-name', 'sub-collection-optional')
  .then(data => console.log(JSON.stringify(data)));
*/


firestoreService
  .backups(['hosts']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Hosts.json", collections);
  });

firestoreService
  .backups(['users']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Users.json", collections);
  });

firestoreService
  .backups(['roles']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Roles.json", collections);
  });

firestoreService
  .backups(['referencedata']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections
    saveData("./seed_data/collections/RefData.json", collections);
  });

firestoreService
  .backups(['auth']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Auth.json", collections);
  });

firestoreService
  .backups(['hosts']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Hosts.json", collections);
  });

firestoreService
  .backups(['cities']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Cities.json",collections);
  });

firestoreService
  .backups(['courses']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Courses.json",collections);
  });

firestoreService
  .backups(['enrollments']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Enrollments.json",collections);
  });

firestoreService
  .backups(['events']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Events.json",collections);
  });

firestoreService
  .backups(['feedback']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Feedback.json",collections);
  });

firestoreService
  .backups(['files']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Files.json",collections);
  });

firestoreService
  .backups(['glossary']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Glossary.json",collections);
  });


firestoreService
  .backups(['photos']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Photos.json",collections);
  });

firestoreService
  .backups(['pictures']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Pictures.json",collections);
  });

firestoreService
  .backups(['questionnaires']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Questionnaires.json",collections);
  });


firestoreService
  .backups(['user_activities.json']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/User_activities.json",collections);
  });

firestoreService
  .backups(['states']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/States.json",collections);
  });

firestoreService
  .backups(['permissions']) // Array of collection's name is OPTIONAL
  .then(collections => {
    // You can do whatever you want with collections

    saveData("./seed_data/collections/Permissions.json",collections);
  });

function saveData(collectionFile, data) {
  // stringify JSON Object
  var jsonContent = JSON.stringify(data);

  fs.writeFile(collectionFile, jsonContent, 'utf8', function (err) {
    if (err) {
      console.log("An error occured while writing JSON Object to File.");
      return console.log(err);
    }

    console.log("JSON file has been saved.");
  });
}
